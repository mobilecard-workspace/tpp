package mx.mobilecard.tpp.ui.recarga.seleccion

import androidx.lifecycle.ViewModel
import io.reactivex.Completable
import mx.mobilecard.crypto.AddcelCrypto
import mx.mobilecard.tpp.data.network.tpp.TebcaCardEntity
import timber.log.Timber

/**
 * ADDCEL on 2019-11-26.
 */
class RecargaSeleccionViewModel : ViewModel() {

    val originalCards: MutableList<TebcaCardEntity> = mutableListOf()
    val filteredCards: MutableList<TebcaCardEntity> = mutableListOf()
    val oldFilteredCards: MutableList<TebcaCardEntity> = mutableListOf()

    fun search(query: String): Completable = Completable.create {
        val wanted = originalCards.filter { card ->
            card.nombre.contains(
                query,
                true
            ) || AddcelCrypto.decryptHard(card.pan) == query || query == card.dni
        }.toList()

        Timber.d("FILTRO A APLICAR: %S", query)
        Timber.d("RESULTADO APLICACION FILTRO: %S", wanted.toString())

        filteredCards.clear()
        filteredCards.addAll(wanted)
        it.onComplete()
    }

}