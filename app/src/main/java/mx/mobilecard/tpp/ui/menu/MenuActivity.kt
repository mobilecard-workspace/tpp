package mx.mobilecard.tpp.ui.menu

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_menu.*
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.TppApp
import mx.mobilecard.tpp.ui.BaseView
import mx.mobilecard.tpp.ui.enroll.form.EnrollFormActivity
import mx.mobilecard.tpp.ui.historial.HistorialActivity
import mx.mobilecard.tpp.ui.login.LoginActivity
import mx.mobilecard.tpp.ui.perfil.PerfilActivity
import mx.mobilecard.tpp.ui.recarga.seleccion.RecargaSeleccionActivity


interface MenuView : BaseView {

    fun launchEnroll()

    fun launchRecarga()

    fun launchHistorial()

    fun launchPerfil()

    fun clickSoporte()

    fun createLlamadaDialog(context: Context): AlertDialog

    fun launchLlamada()

    fun clickLogout()

    fun onLogout()
}

class MenuActivity : AppCompatActivity(), MenuView {

    companion object {
        private const val SUPPORT_PHONE = "tel:080080102"
        fun get(context: Context): Intent {
            return Intent(context, MenuActivity::class.java)
        }
    }

    private lateinit var presenter: MenuPresenter
    private lateinit var llamadaDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter = MenuPresenterImpl(TppApp.get().userRepository, CompositeDisposable(), this)

        setContentView(R.layout.activity_menu)

        b_menu_enrolar.setOnClickListener {
            launchEnroll()
        }

        b_menu_existente.setOnClickListener {
            launchRecarga()
        }

        b_menu_historial.setOnClickListener { launchHistorial() }

        b_menu_perfil.setOnClickListener {
            launchPerfil()
        }

        b_menu_soporte.setOnClickListener {
            clickSoporte()
        }

        b_menu_logout.setOnClickListener {
            clickLogout()
        }
    }

    override fun launchEnroll() {
        startActivity(Intent(this@MenuActivity, EnrollFormActivity::class.java))
    }

    override fun launchRecarga() {
        startActivity(RecargaSeleccionActivity.get(this))
    }

    override fun launchHistorial() {
        startActivity(HistorialActivity.get(this))
    }

    override fun launchPerfil() {
        startActivity(PerfilActivity.get(this))
    }

    override fun clickSoporte() {
        if (!::llamadaDialog.isInitialized) {
            llamadaDialog = createLlamadaDialog(this@MenuActivity)
        }

        if (!llamadaDialog.isShowing) llamadaDialog.show()
    }

    override fun createLlamadaDialog(context: Context): AlertDialog {
        return AlertDialog.Builder(this).setTitle("Información")
            .setMessage("¿Deseas comunicarte con el equipo de soporte?")
            .setPositiveButton("Llamar") { d, _ ->
                launchLlamada()
                d.dismiss()
            }
            .setNegativeButton("Cancelar") { d, _ ->
                d.dismiss()
            }.create()
    }

    override fun launchLlamada() {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse(SUPPORT_PHONE)
        startActivity(intent)
    }

    override fun clickLogout() {
        AlertDialog.Builder(this).setTitle("Cerrar sesión")
            .setMessage("¿Desea finalizar la sesión Tpp?")
            .setPositiveButton("Cerrar") { d, _ ->
                presenter.logout()
                d.dismiss()
            }
            .setNegativeButton("Cancelar") { d, _ ->
                d.dismiss()
            }.show()
    }

    override fun onLogout() {
        val intent = LoginActivity.get(this).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        finish()
    }

    override fun showProgress() {
        progress_menu.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_menu.visibility = View.GONE
    }

    override fun onError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun onSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }
}
