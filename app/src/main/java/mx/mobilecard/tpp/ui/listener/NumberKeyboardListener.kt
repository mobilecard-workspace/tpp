package mx.mobilecard.tpp.ui.listener

import java.text.NumberFormat
import java.util.*

/**
 * ADDCEL on 2019-11-26.
 */
interface NumberKeyboardListener {

    companion object {

        private val localeMx = Locale("es", "MX")
        private val localeCol = Locale("es", "CO")
        private val localeUsa = Locale.US
        private val localePer = Locale("es", "PE")

        const val PATTERN_DECIMAL = "^\\d{1,6}(\\.\\d{1,2})?\$"

        fun getFormattedMonto(monto: String, idPais: Int): String {
            return if (monto.matches(PATTERN_DECIMAL.toRegex())) {
                val locale = getLocaleByIdPais(idPais)
                val formatter = NumberFormat.getCurrencyInstance(locale)
                formatter.format(processMonto(monto))
            } else {
                ""
            }
        }

        fun getFormattedMontoDecimal(monto: String, idPais: Int): String {
            val locale = getLocaleByIdPais(idPais)
            val formatter = NumberFormat.getCurrencyInstance(locale)
            return formatter.format(processMontoDecimal(monto))
        }

        fun getFormattedMontoFromDouble(monto: Double, idPais: Int): String {
            val locale = getLocaleByIdPais(idPais)
            val formatter = NumberFormat.getCurrencyInstance(locale)
            return formatter.format(monto)
        }

        private fun getLocaleByIdPais(idPais: Int): Locale {
            return when (idPais) {
                1 -> localeMx
                2 -> localeCol
                3 -> localeUsa
                4 -> localePer
                else -> {
                    localeUsa
                }
            }
        }

        private fun processMonto(monto: String): Double {
            return monto.toDouble() / 100
        }

        private fun processMontoDecimal(monto: String): Double {
            if (monto.isEmpty()) return 0.0
            return monto.toDouble()
        }
    }

    fun onKeyStroke(captured: String)

    fun onDeleteStroke()
}