package mx.mobilecard.tpp.ui.recarga.result

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_result.*
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.data.network.tpp.RecargaEntity
import mx.mobilecard.tpp.ui.listener.NumberKeyboardListener
import mx.mobilecard.tpp.ui.menu.MenuActivity

@Parcelize
data class RecargaResultViewModel(val result: RecargaEntity) : Parcelable

interface RecargaResultView {
    fun setResultImg()

    fun setResultTitle()

    fun setResultDetail()

    fun clickShare()

    fun clickInicio()

    fun showRecarga()

    fun clickRecarga()
}

class RecargaResultActivity : AppCompatActivity(), RecargaResultView {

    companion object {
        fun get(context: Context, result: RecargaEntity): Intent {
            return Intent(context, RecargaResultActivity::class.java).putExtra(
                "model",
                RecargaResultViewModel(result)
            )
        }
    }

    lateinit var model: RecargaResultViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        model = intent?.getParcelableExtra("model")!!

        setResultImg()
        setResultTitle()
        setResultDetail()
        showRecarga()

        b_result_share.setOnClickListener { clickShare() }
        b_result_inicio.setOnClickListener { clickInicio() }
        b_result_recargar.setOnClickListener { clickRecarga() }
    }

    override fun setResultImg() {
        val img = if (model.result.idError == 0) R.drawable.exito_icon else R.drawable.error_icon
        img_result.setImageResource(img)
    }

    override fun setResultTitle() {
        text_result_title.text = model.result.mensajeError
    }

    override fun setResultDetail() {
        if (model.result.idError == 0) {
            text_result_detail.text = buildDetail(model.result)
        } else {
            text_result_detail.visibility = View.GONE
        }
    }

    override fun clickShare() {

    }

    override fun clickInicio() {
        val intent = MenuActivity.get(this).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    override fun showRecarga() {
        b_result_recargar.visibility = View.GONE
    }

    override fun clickRecarga() {

    }

    private fun buildDetail(result: RecargaEntity): CharSequence {
        return TextUtils.concat(
            getDetailSpan("Nombre:", result.nombreDestino),
            "\n\n",
            getDetailSpan("Fecha:", result.fecha),
            "\n\n",
            getDetailSpan("Tarjeta:", result.tarjejaDestino),
            "\n\n",
            getDetailSpan("Autorización:", result.autorizacion),
            "\n\n",
            getDetailSpan(
                "Monto:",
                NumberKeyboardListener.getFormattedMontoFromDouble(result.monto, 4)
            )
        )

    }

    private fun getDetailSpan(key: String, value: String): CharSequence {
        val firstPart = SpannableString(key)
        val lastPart = SpannableString(value)

        firstPart.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)), 0,
            firstPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        lastPart.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorFontLight)), 0,
            lastPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        return TextUtils.concat(firstPart, " ", lastPart)
    }
}
