package mx.mobilecard.tpp.ui.menu

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import mx.mobilecard.tpp.data.local.user.UserRepository
import mx.mobilecard.tpp.ui.BasePresenter

/**
 * ADDCEL on 2019-11-25.
 */
interface MenuPresenter : BasePresenter {
    fun logout()
}

class MenuPresenterImpl(
    val repository: UserRepository,
    val compositeDisposable: CompositeDisposable,
    val view: MenuView
) : MenuPresenter {
    override fun logout() {
        repository.delete()
        view.onLogout()
    }

    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

}