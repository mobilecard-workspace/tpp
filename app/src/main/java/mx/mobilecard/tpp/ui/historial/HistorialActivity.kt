package mx.mobilecard.tpp.ui.historial

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_historial.*
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.ui.historial.altas.HistorialAltasFragment
import mx.mobilecard.tpp.ui.historial.recargas.HistorialRecargasFragment

class HistorialActivity : AppCompatActivity() {

    companion object {
        fun get(context: Context): Intent {
            return Intent(context, HistorialActivity::class.java)
        }
    }

    var prevMenuItem: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_historial)
        setupViewPager()
        setupNavigationView()

    }

    fun setupViewPager() {
        val adapter = HistorialPagerAdapter(supportFragmentManager)
        val altasFragment = HistorialAltasFragment()
        val recargasFragment = HistorialRecargasFragment()

        adapter.addFragment(altasFragment)
        adapter.addFragment(recargasFragment)

        pager_historial.adapter = adapter

        pager_historial.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                if (prevMenuItem != null) {
                    prevMenuItem!!.isChecked = false
                } else {
                    navigation_historial.menu.getItem(0).isChecked = false
                }
                navigation_historial.menu.getItem(position).isChecked = true
                prevMenuItem = navigation_historial.menu.getItem(position)
            }

        })

    }

    fun setupNavigationView() {
        navigation_historial.setOnNavigationItemSelectedListener { p0 ->
            when (p0.itemId) {
                R.id.action_altas -> pager_historial.currentItem = 0
                R.id.action_recargas -> pager_historial.currentItem = 1
            }
            false
        }
    }
}
