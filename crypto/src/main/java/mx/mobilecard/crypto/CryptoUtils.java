package mx.mobilecard.crypto;

final class CryptoUtils {

    private CryptoUtils() {

    }

    public static synchronized String parsePass(String pass) {
        int len = pass.length();
        StringBuilder key = new StringBuilder();

        for (int i = 0; i < 32 / len; i++) {
            key.append(pass);
        }

        int carry = 0;
        while (key.length() < 32) {
            key.append(pass.charAt(carry));
            carry++;
        }
        return key.toString();
    }

    private static synchronized String reverse(String chain) {
        StringBuilder nuevo = new StringBuilder();

        for (int i = chain.length() - 1; i >= 0; i--) {
            nuevo.append(chain.charAt(i));
        }

        return nuevo.toString();
    }

    public static synchronized String mergeStr(String first, String second) {
        StringBuilder result = new StringBuilder();
        String other = reverse(second);

        result.append((Integer.toString(other.length()).length() < 2) ? "0" + other.length()
                : Integer.toString(other.length()));

        String sub1 = first.substring(0, 19);
        String sub2 = first.substring(19);

        result.append(sub1);

        for (int i = 0; i < other.length(); i += 2) {
            int offset = ((i + 2) <= other.length()) ? (i + 2) : (i + 1);
            String next = other.substring(i, offset);

            result.append(next);

            result.append(sub2, 0, 2);

            sub2 = sub2.substring(2);
        }
        result.append(sub2);

        return result.toString();
    }
}
