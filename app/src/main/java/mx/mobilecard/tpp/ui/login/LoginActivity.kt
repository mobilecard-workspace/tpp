package mx.mobilecard.tpp.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_login.*
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.TppApp
import mx.mobilecard.tpp.data.network.tpp.LoginEntity
import mx.mobilecard.tpp.data.network.tpp.TppAPI
import mx.mobilecard.tpp.ui.BaseView
import mx.mobilecard.tpp.ui.menu.MenuActivity
import mx.mobilecard.tpp.utils.AndroidUtils
import mx.mobilecard.tpp.utils.ValidationUtils


interface LoginView : BaseView, TextWatcher {

    fun onEmailError(msg: String)

    fun onPasswordError(msg: String)

    fun onFormSuccess()

    fun clickLogin()

    fun onLogin(result: LoginEntity)
}

class LoginActivity : AppCompatActivity(), LoginView {

    private val tppAPI = TppAPI.get(TppApp.get().retrofit)
    private val userRepository = TppApp.get().userRepository

    lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = LoginPresenterImpl(tppAPI, userRepository, CompositeDisposable(), this)
        setContentView(R.layout.activity_login)

        text_correo.addTextChangedListener(this)
        text_pass.addTextChangedListener(this)

        b_login_iniciar.setOnClickListener {
            clickLogin()
        }

    }

    override fun onDestroy() {
        text_correo.removeTextChangedListener(this)
        text_pass.removeTextChangedListener(this)
        presenter.clearDisposables()
        super.onDestroy()
    }

    companion object {
        fun get(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

    override fun onEmailError(msg: String) {
        text_correo.error = msg
    }

    override fun onPasswordError(msg: String) {
        text_pass.error = msg
    }

    override fun onFormSuccess() {
        ValidationUtils.clearViewErrors(text_correo, text_pass)

        val login = AndroidUtils.getText(text_correo)
        val pass = AndroidUtils.getText(text_pass)

        presenter.login(login, pass)
    }

    override fun clickLogin() {

        val login = AndroidUtils.getText(text_correo)
        val pass = AndroidUtils.getText(text_pass)

        presenter.evalForm(login, pass)
    }

    override fun onLogin(result: LoginEntity) {
        startActivity(MenuActivity.get(this@LoginActivity))
        finish()
    }

    override fun showProgress() {
        progress_login.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_login.visibility = View.GONE
    }

    override fun onError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun onSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun afterTextChanged(s: Editable?) {
        if (s != null) {
            if (text_correo.hasFocus()) {
                if (Patterns.EMAIL_ADDRESS.matcher(s).matches()) {
                    text_correo.error = null
                } else {
                    text_correo.error = "El correo ingresado no es válido"
                }
            }
            if (text_pass.hasFocus()) {
                if (s.isNotEmpty() && s.length >= 8) {
                    text_pass.error = null
                } else {
                    text_pass.error = "La contraseña ingresada no es válida"
                }
            }
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }
}
