package mx.mobilecard.tpp.ui.historial.recargas

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.screen_historial.*
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.TppApp
import mx.mobilecard.tpp.data.network.tpp.HistorialRecargaEntity
import mx.mobilecard.tpp.data.network.tpp.TppAPI
import mx.mobilecard.tpp.ui.BaseView

/**
 * ADDCEL on 2019-11-27.
 */

interface HistorialRecargasView : BaseView {
    fun launchRecargas()
    fun setRecargas(recargas: List<HistorialRecargaEntity>)
}


class HistorialRecargasFragment : Fragment(), HistorialRecargasView {

    val api = TppAPI.get(TppApp.get().retrofit)
    val repository = TppApp.get().userRepository
    val adapter = HistorialRecargasAdapter(ArrayList())
    lateinit var presenter: HistorialRecargasPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = HistorialRecargasPresenterImpl(api, repository, view = this)
        launchRecargas()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_historial, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_historial.adapter = adapter
        recycler_historial.layoutManager = LinearLayoutManager(view.context)
        recycler_historial.addItemDecoration(
            DividerItemDecoration(
                view.context,
                DividerItemDecoration.VERTICAL
            )
        )
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun launchRecargas() {
        presenter.getRecargas()
    }

    override fun setRecargas(recargas: List<HistorialRecargaEntity>) {
        adapter.update(recargas)
    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun onError(msg: String) {
        activity?.let { Toasty.error(it, msg).show() }
    }

    override fun onSuccess(msg: String) {
        activity?.let { Toasty.success(it, msg).show() }
    }


}