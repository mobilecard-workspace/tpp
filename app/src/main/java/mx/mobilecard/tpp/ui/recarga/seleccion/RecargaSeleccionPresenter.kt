package mx.mobilecard.tpp.ui.recarga.seleccion

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import mx.mobilecard.tpp.BuildConfig
import mx.mobilecard.tpp.data.local.user.UserRepository
import mx.mobilecard.tpp.data.network.tpp.TppAPI
import mx.mobilecard.tpp.ui.BasePresenter
import mx.mobilecard.tpp.utils.ErrorUtils
import mx.mobilecard.tpp.utils.StringUtil

/**
 * ADDCEL on 2019-11-26.
 */
interface RecargaSeleccionPresenter : BasePresenter {

    val compositeDisposable: CompositeDisposable

    fun getCards()

    fun downloadCards()
}

class RecargaSeleccionPresenterImpl(
    val api: TppAPI,
    val repository: UserRepository,
    override val compositeDisposable: CompositeDisposable = CompositeDisposable(),
    val view: RecargaSeleccionView
) : RecargaSeleccionPresenter {

    override fun getCards() {

        val user = repository.read()
        if (user != null) {
            view.showProgress()
            val cDisp = api.getCards(
                BuildConfig.ADDCEL_APP_ID, 4,
                StringUtil.getCurrentLanguage(), TppAPI.TIPO_OPERADOR, user.id
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.idError == 0) {
                        view.setCards(it.cards)
                    } else {
                        view.onError(it.mensajeError)
                    }
                },
                    {
                        view.hideProgress()
                        view.onError(ErrorUtils.getFormattedHttpErrorMsg(it))
                    })
            addToDisposables(cDisp)
        }
    }

    override fun downloadCards() {
    }

    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }
}