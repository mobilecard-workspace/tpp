package mx.mobilecard.tpp.utils

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.text.format.DateFormat
import android.view.View
import java.io.File
import java.io.FileOutputStream
import java.util.*

/**
 * ADDCEL on 2019-12-04.
 */
object ScreenshotUtils {


    private const val SCREENSHOT_PATH = "/Pictures/Screenshots"
    val PERMISSIONS = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    fun shareScreenshot(activity: Activity?, title: String): Intent? {
        return if (activity != null) {
            val rootView: View = activity.window.decorView.findViewById(android.R.id.content)
            val screenshotAsBitmap = getScreenshot(rootView)
            val file = store(screenshotAsBitmap, buildScreenshotName())
            getImageIntent(file, title)
        } else {
            null
        }
    }

    private fun getScreenshot(view: View): Bitmap {
        val screenView = view.rootView
        screenView.isDrawingCacheEnabled = true
        val bitmap = Bitmap.createBitmap(screenView.drawingCache)
        screenView.isDrawingCacheEnabled = false
        return bitmap
    }

    private fun store(bm: Bitmap, fileName: String): File {
        val path = Environment.getExternalStorageDirectory().absolutePath + SCREENSHOT_PATH
        val dir = File(path)
        if (!dir.exists()) dir.mkdirs()
        val file = File(path, fileName)
        try {
            val fOut = FileOutputStream(file)
            bm.compress(Bitmap.CompressFormat.PNG, 85, fOut)
            fOut.flush()
            fOut.close()
        } catch (e: Throwable) {
            e.printStackTrace()
        }
        return file
    }

    private fun buildScreenshotName(): String {
        val fecha = DateFormat.format("yyyyMMddHHmmss", Calendar.getInstance()).toString()
        return "transaction_$fecha.png"
    }

    private fun getImageIntent(f: File, title: String): Intent {
        val uri = Uri.fromFile(f)
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_SUBJECT, "")
        intent.putExtra(Intent.EXTRA_TEXT, "")
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        return Intent.createChooser(intent, title)

    }
}
