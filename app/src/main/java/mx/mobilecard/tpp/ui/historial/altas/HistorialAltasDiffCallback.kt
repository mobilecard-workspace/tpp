package mx.mobilecard.tpp.ui.historial.altas

import androidx.recyclerview.widget.DiffUtil
import mx.mobilecard.tpp.data.network.tpp.HistorialAltaEntity

/**
 * ADDCEL on 2019-12-02.
 */
class HistorialAltasDiffCallback(
    private val oldList: List<HistorialAltaEntity>,
    private val newList: List<HistorialAltaEntity>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        return super.getChangePayload(oldItemPosition, newItemPosition)
    }
}