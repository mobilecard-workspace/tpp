package mx.mobilecard.tpp.ui.enroll.form

import android.app.DatePickerDialog
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.vvalidator.form
import es.dmoral.toasty.Toasty
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_enroll_form.*
import mx.mobilecard.crypto.AddcelCrypto
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.TppApp
import mx.mobilecard.tpp.data.network.catalogos.CatalogoAPI
import mx.mobilecard.tpp.data.network.catalogos.DeptoEntity
import mx.mobilecard.tpp.data.network.catalogos.DistritoEntity
import mx.mobilecard.tpp.data.network.catalogos.ProvinciaEntity
import mx.mobilecard.tpp.data.network.tpp.TebcaAltaRequest
import mx.mobilecard.tpp.data.network.tpp.TebcaAltaResponse
import mx.mobilecard.tpp.data.network.tpp.TppAPI
import mx.mobilecard.tpp.ui.RetryView
import mx.mobilecard.tpp.ui.enroll.result.EnrollResultActivity
import mx.mobilecard.tpp.ui.utils.FixedHoloDatePickerDialog
import mx.mobilecard.tpp.utils.AndroidUtils
import mx.mobilecard.tpp.utils.StringUtil
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.util.*


interface EnrollmentFormView : RetryView {

    fun setInitialValues(
        deptos: List<DeptoEntity>,
        provincias: List<ProvinciaEntity>,
        distritos: List<DistritoEntity>
    )

    fun configAdapters()

    fun configSpinners()

    fun setDeptos(deptos: List<DeptoEntity>)

    fun setProvincias(provincias: List<ProvinciaEntity>, currDeptoPos: Int)

    fun setDistritos(distritos: List<DistritoEntity>, currProvinciaPos: Int)

    fun buildNacimientoDialog()

    fun showNacimientoDialog()

    fun buildVigenciaDialog()

    fun showVigenciaDialog()

    fun fetchGenero(): String

    fun disableEnroll()

    fun enableEnroll()

    fun launchEnrolar(
        tdc: String,
        vigencia: String,
        email: String,
        dni: String,
        verificador: String,
        nombre: String,
        paterno: String,
        materno: String,
        nacimiento: String,
        genero: String,
        direccion: String,
        depto: DeptoEntity,
        provincia: ProvinciaEntity,
        distrito: DistritoEntity,
        telefono: String,
        centroLaboral: String,
        ocupacion: String,
        servidorPublico: Boolean,
        institucion: String,
        cargoPublico: String
    )

    fun onEnroll(result: TebcaAltaResponse)
}

class EnrollFormActivity : AppCompatActivity(), EnrollmentFormView {


    companion object {
        private val dobFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        private val expiryFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/yy")
        private const val PATTERN_VISA = "^(4)(\\d{12}|\\d{15})$"
        private const val PATTERN_TEBCA = "^(?=\\d{16}$)(483039)\\d+"
        private const val PATTERN_DNI = "^\\d{8}(?:[-\\s]\\d{4})?\$"
    }

    private val retrofit = TppApp.get().retrofit
    private val api = TppAPI.get(retrofit)
    private val catalogo = CatalogoAPI.get(retrofit)
    private val repo = TppApp.get().userRepository
    private val disposables = CompositeDisposable()

    lateinit var presenter: EnrollFormPresenter
    lateinit var nacimientoDateTime: LocalDateTime
    lateinit var nacimientoDialog: DatePickerDialog

    lateinit var deptoAdapter: ArrayAdapter<DeptoEntity>
    lateinit var provinciaAdapter: ArrayAdapter<ProvinciaEntity>
    lateinit var distritoAdapter: ArrayAdapter<DistritoEntity>

    lateinit var vigenciaDateTime: LocalDateTime
    lateinit var vigenciaDialog: DatePickerDialog

    //ESTADO ACTUAL DEPARTAMENTO y PROVINCIAS para no volver a llamar en caso de seleccionar el mismo
    var currDeptoPos: Int = -1
    var currProvinciaPos: Int = -1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enroll_form)
        presenter = EnrollFormPresenterImpl(api, catalogo, repo, this)
        configRetry(retry_enroll)
        buildVigenciaDialog()
        buildNacimientoDialog()

        form {
            useRealTimeValidation()

            input(R.id.til_enroll_tdc, name = "Tarjeta bancaria") {
                val luhn = StringUtil.lunhCheck(AndroidUtils.getText(til_enroll_tdc))
                isNotEmpty().description(R.string.error_card)
                isNumber().description(R.string.error_card)
                conditional({ luhn }) {
                    matches(PATTERN_TEBCA).description(R.string.error_card)
                }

            }
            input(R.id.til_enroll_vigencia, name = "Vigencia") {
                isNotEmpty().description(R.string.error_vigencia)
            }
            input(R.id.til_enroll_nombre, name = "Nombre") {
                isNotEmpty().description(R.string.error_empty)
            }
            input(R.id.til_enroll_paterno, name = "Apellido paterno") {
                isNotEmpty().description(R.string.error_empty)
            }
            input(R.id.til_enroll_materno, name = "Apellido materno") {
                isNotEmpty().description(R.string.error_empty)
            }
            input(R.id.til_enroll_email, name = "Correo electrónico") {
                isEmail().description(R.string.error_email)
            }
            input(R.id.til_enroll_dni, name = "DNI") {
                isNotEmpty().description(R.string.error_empty)
                isNumber().description(R.string.error_dni)
                length().exactly(8).description(R.string.error_dni)
            }
            input(R.id.til_enroll_verificador, name = "Verificador") {
                isNotEmpty().description(R.string.error_empty)
                isNumber()
            }
            input(R.id.til_enroll_nacimiento, name = "Fecha de nacimiento") {
                isNotEmpty().description(R.string.error_empty)
            }
            input(R.id.til_enroll_direccion, name = "Direccion") {
                isNotEmpty().description(R.string.error_empty)
            }
            input(R.id.til_enroll_telefono, name = "Telefono") {
                isNotEmpty().description(R.string.error_empty)
                isNumber().description("El campo debe ser numérico")
                length().exactly(9).description("Teléfono no válido")
            }
            input(R.id.til_enroll_centrolaboral, name = "Centro laboral") {
                isNotEmpty().description(R.string.error_empty)
            }
            input(R.id.til_enroll_ocupacion, name = "Ocupación") {
                isNotEmpty().description(R.string.error_empty)
            }
            input(R.id.til_enroll_institucion, name = "Institucion") {
                conditional({ check_enroll_burocrata.isChecked }) {
                    isNotEmpty().description(R.string.error_empty)
                }
            }
            input(R.id.til_enroll_cargopublico, name = "Cargo público") {
                conditional({ check_enroll_burocrata.isChecked }) {
                    isNotEmpty().description(R.string.error_empty)
                }
            }

            submitWith(R.id.b_enroll_enrolar) {
                launchEnrolar(
                    AndroidUtils.getText(til_enroll_tdc),
                    expiryFormatter.format(vigenciaDateTime),
                    AndroidUtils.getText(til_enroll_email),
                    AndroidUtils.getText(til_enroll_dni),
                    AndroidUtils.getText(til_enroll_verificador),
                    StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_enroll_nombre)),
                    StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_enroll_paterno)),
                    StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_enroll_materno)),
                    dobFormatter.format(nacimientoDateTime),
                    fetchGenero(),
                    StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_enroll_direccion)),
                    spinner_enroll_depto.selectedItem as DeptoEntity,
                    spinner_enroll_provincia.selectedItem as ProvinciaEntity,
                    spinner_enroll_distrito.selectedItem as DistritoEntity,
                    StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_enroll_telefono)),
                    StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_enroll_centrolaboral)),
                    StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_enroll_ocupacion)),
                    check_enroll_burocrata.isChecked,
                    if (check_enroll_burocrata.isChecked) StringUtil.removeAcentosMultiple(
                        AndroidUtils.getText(til_enroll_institucion)
                    ) else "",
                    if (check_enroll_burocrata.isChecked) StringUtil.removeAcentosMultiple(
                        AndroidUtils.getText(til_enroll_cargopublico)
                    ) else ""
                )
            }
        }

        configAdapters()
        configSpinners()

        til_enroll_vigencia.setOnClickListener { showVigenciaDialog() }
        til_enroll_nacimiento.setOnClickListener { showNacimientoDialog() }
        check_enroll_burocrata.setOnCheckedChangeListener { _, _ ->
            AndroidUtils.setText(til_enroll_institucion, "")
            AndroidUtils.setText(til_enroll_cargopublico, "")
        }

        disposables.add(presenter.loadInitialData())
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    override fun showProgress() {
        if (progress_enroll_form.visibility == View.GONE) progress_enroll_form.visibility =
            View.VISIBLE
    }

    override fun hideProgress() {
        if (progress_enroll_form.visibility == View.VISIBLE) progress_enroll_form.visibility =
            View.GONE
    }

    override fun onError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun onSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun setInitialValues(
        deptos: List<DeptoEntity>,
        provincias: List<ProvinciaEntity>,
        distritos: List<DistritoEntity>
    ) {
        deptoAdapter.addAll(deptos)
        provinciaAdapter.addAll(provincias)
        distritoAdapter.addAll(distritos)
    }

    override fun configAdapters() {
        deptoAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item)
        deptoAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        deptoAdapter.setNotifyOnChange(true)

        provinciaAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item)
        provinciaAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        provinciaAdapter.setNotifyOnChange(true)

        distritoAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item)
        distritoAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        distritoAdapter.setNotifyOnChange(true)
    }

    override fun configSpinners() {
        val generoAdapter = ArrayAdapter(
            this,
            R.layout.support_simple_spinner_dropdown_item,
            listOf("HOMBRE", "MUJER")
        )
        generoAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spinner_enroll_genero.adapter = generoAdapter

        spinner_enroll_depto.adapter = deptoAdapter
        spinner_enroll_provincia.adapter = provinciaAdapter
        spinner_enroll_distrito.adapter = distritoAdapter

        spinner_enroll_depto.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                    provinciaAdapter.clear()
                    distritoAdapter.clear()
                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    if (p2 != currDeptoPos) {
                        val it = deptoAdapter.getItem(p2)
                        if (it != null) {
                            provinciaAdapter.clear()
                            distritoAdapter.clear()
                            disposables.add(presenter.getProvincia(it, currDeptoPos))
                        }
                    }
                }
            }

        spinner_enroll_provincia.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                    distritoAdapter.clear()
                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    if (p2 != currProvinciaPos) {
                        val it = provinciaAdapter.getItem(p2)
                        if (it != null) {
                            distritoAdapter.clear()
                            disposables.add(presenter.getDistrito(it, currProvinciaPos))
                        }
                    }
                }
            }
    }

    override fun setDeptos(deptos: List<DeptoEntity>) {
        if (deptoAdapter.count > 0) deptoAdapter.clear()
        deptoAdapter.addAll(deptos)
        if (deptoAdapter.count > 0) {
            this.currDeptoPos = 0
            disposables.add(
                presenter.getProvincia(
                    deptoAdapter.getItem(currDeptoPos)!!,
                    currDeptoPos
                )
            )
        }
    }

    override fun setProvincias(provincias: List<ProvinciaEntity>, currDeptoPos: Int) {
        if (provinciaAdapter.count > 0) provinciaAdapter.clear()
        provinciaAdapter.addAll(provincias)
        if (provinciaAdapter.count > 0) {
            this.currDeptoPos = currDeptoPos
            currProvinciaPos = 0
            disposables.add(
                presenter.getDistrito(
                    provinciaAdapter.getItem(currProvinciaPos)!!,
                    currProvinciaPos
                )
            )
        }
    }

    override fun setDistritos(distritos: List<DistritoEntity>, currProvinciaPos: Int) {
        if (distritoAdapter.count > 0) distritoAdapter.clear()
        distritoAdapter.addAll(distritos)
        if (distritoAdapter.count > 0) {
            this.currProvinciaPos = currProvinciaPos
        }
    }

    override fun buildNacimientoDialog() {
        val listener = DatePickerDialog.OnDateSetListener { _, i, i1, i2 ->
            val time = LocalDateTime.of(i, i1 + 1, i2, 0, 0)
            nacimientoDateTime = time
            AndroidUtils.setText(til_enroll_nacimiento, dobFormatter.format(nacimientoDateTime))

        }

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            nacimientoDialog = FixedHoloDatePickerDialog(
                this, listener,
                Calendar.getInstance().get(Calendar.YEAR) - 18,
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
            )
        } else {
            nacimientoDialog =
                DatePickerDialog(
                    this, R.style.CustomDatePickerDialogTheme, listener,
                    Calendar.getInstance().get(Calendar.YEAR) - 18,
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                )
        }

        nacimientoDialog.setTitle("Fecha de nacimiento")
    }

    override fun showNacimientoDialog() {
        if (::nacimientoDialog.isInitialized) {
            nacimientoDialog.show()
        } else {
            buildNacimientoDialog()
            nacimientoDialog.show()
        }
    }


    override fun buildVigenciaDialog() {
        val now = Calendar.getInstance()

        val vigenciaListener = DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            val time = LocalDateTime.of(year, month + 1, dayOfMonth, 0, 0)
            vigenciaDateTime = time
            AndroidUtils.setText(til_enroll_vigencia, expiryFormatter.format(vigenciaDateTime))
        }

        vigenciaDialog = if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            FixedHoloDatePickerDialog(
                this, vigenciaListener, now.get(Calendar.YEAR),
                now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH)
            )
        } else {
            DatePickerDialog(
                this, R.style.CustomDatePickerDialogTheme, vigenciaListener,
                now.get(Calendar.YEAR), now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
            )
        }

        vigenciaDialog.datePicker.findViewById<View>(
            Resources.getSystem().getIdentifier(
                "day",
                "id",
                "android"
            )
        ).visibility = View.GONE

        vigenciaDialog.setTitle("Vigencia tarjeta")
    }

    override fun showVigenciaDialog() {
        if (::vigenciaDialog.isInitialized) {
            vigenciaDialog.show()
        } else {
            buildVigenciaDialog()
            vigenciaDialog.show()
        }
    }

    override fun fetchGenero(): String {
        return when (spinner_enroll_genero.selectedItemPosition) {
            0 -> "M"
            1 -> "F"
            else -> ""
        }
    }

    override fun disableEnroll() {
        b_enroll_enrolar.isEnabled = false
    }

    override fun enableEnroll() {
        b_enroll_enrolar.isEnabled = true
    }

    override fun launchEnrolar(
        tdc: String,
        vigencia: String,
        email: String,
        dni: String,
        verificador: String,
        nombre: String,
        paterno: String,
        materno: String,
        nacimiento: String,
        genero: String,
        direccion: String,
        depto: DeptoEntity,
        provincia: ProvinciaEntity,
        distrito: DistritoEntity,
        telefono: String,
        centroLaboral: String,
        ocupacion: String,
        servidorPublico: Boolean,
        institucion: String,
        cargoPublico: String
    ) {
        val body = TebcaAltaRequest(
            materno,
            paterno,
            verificador,
            dni,
            email,
            nacimiento,
            genero,
            nombre,
            AddcelCrypto.encryptHard(tdc),
            AddcelCrypto.encryptHard(vigencia),
            direccion, depto.codigo,
            provincia.codigo,
            distrito.codigo,
            telefono,
            centroLaboral,
            ocupacion,
            if (servidorPublico) "1" else "0",
            cargoPublico,
            institucion
        )
        disposables.add(presenter.enrollCard(body))
    }

    override fun onEnroll(result: TebcaAltaResponse) {
        startActivity(EnrollResultActivity.get(this, result))
        if (result.idError == 0) finish()
    }

    override fun configRetry(view: View) {
        view.findViewById<Button>(R.id.b_retry).setOnClickListener {
            clickRetry()
        }
    }

    override fun showRetry() {
        if (retry_enroll.visibility == View.GONE) retry_enroll.visibility = View.VISIBLE
    }

    override fun hideRetry() {
        if (retry_enroll.visibility == View.VISIBLE) retry_enroll.visibility = View.GONE
    }

    override fun clickRetry() {
        disposables.add(presenter.loadInitialData())
    }

}
