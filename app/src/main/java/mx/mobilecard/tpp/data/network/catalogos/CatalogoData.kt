package mx.mobilecard.tpp.data.network.catalogos

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * ADDCEL on 21/11/19.
 */
@Parcelize
data class DeptoEntity(
    @SerializedName("codigo") val codigo: String, @SerializedName("departamento")
    val departamento: String, @SerializedName("id") val id: Int, @SerializedName("idPais")
    val idPais: Int, @SerializedName("status") val status: Int
) : Parcelable {
    override fun toString(): String {
        return departamento
    }
}

@Parcelize
data class ProvinciaEntity(
    @SerializedName("codigo") val codigo: String,
    @SerializedName("codigoDepartamento") val codigoDepartamento: String, @SerializedName("id")
    val id: Int, @SerializedName("idPais") val idPais: Int, @SerializedName("provincia")
    val provincia: String, @SerializedName("status") val status: Int
) : Parcelable {
    override fun toString(): String {
        return provincia
    }
}

@Parcelize
data class DistritoEntity(
    @SerializedName("codigo") val codigo: String,
    @SerializedName("codigoProvincia") val codigoProvincia: String, @SerializedName("distrito")
    val distrito: String, @SerializedName("id") val id: Int, @SerializedName("idPais")
    val idPais: Int, @SerializedName("status") val status: Int
) : Parcelable {
    override fun toString(): String {
        return distrito
    }
}