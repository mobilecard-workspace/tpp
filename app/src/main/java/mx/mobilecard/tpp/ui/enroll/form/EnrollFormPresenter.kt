package mx.mobilecard.tpp.ui.enroll.form

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import mx.mobilecard.tpp.BuildConfig
import mx.mobilecard.tpp.data.local.user.UserRepository
import mx.mobilecard.tpp.data.network.catalogos.CatalogoAPI
import mx.mobilecard.tpp.data.network.catalogos.DeptoEntity
import mx.mobilecard.tpp.data.network.catalogos.DistritoEntity
import mx.mobilecard.tpp.data.network.catalogos.ProvinciaEntity
import mx.mobilecard.tpp.data.network.tpp.TebcaAltaRequest
import mx.mobilecard.tpp.data.network.tpp.TppAPI
import mx.mobilecard.tpp.ui.BasePresenter
import mx.mobilecard.tpp.utils.ErrorUtils
import mx.mobilecard.tpp.utils.StringUtil

/**
 * ADDCEL on 2019-11-25.
 */
interface EnrollFormPresenter : BasePresenter {
    fun loadInitialData(): Disposable
    fun getDeptos(): Disposable
    fun getProvincia(depto: DeptoEntity, currDeptoPos: Int): Disposable
    fun getDistrito(
        provincia: ProvinciaEntity,
        currProvinciaPos: Int
    ): Disposable

    fun enrollCard(body: TebcaAltaRequest): Disposable
}

class EnrollFormPresenterImpl(
    val api: TppAPI,
    val catalogo: CatalogoAPI,
    val repository: UserRepository,
    val view: EnrollmentFormView
) : EnrollFormPresenter {

    override fun loadInitialData(): Disposable {

        val deptos: MutableList<DeptoEntity> = mutableListOf()
        val provincias: MutableList<ProvinciaEntity> = mutableListOf()
        val distritos: MutableList<DistritoEntity> = mutableListOf()

        view.showProgress()
        view.disableEnroll()

        return catalogo.getDepartamentos(
            BuildConfig.ADDCEL_APP_ID,
            4,
            StringUtil.getCurrentLanguage()
        )
            .subscribeOn(Schedulers.io())
            .flatMap {

                deptos.addAll(it)

                catalogo.getProvincias(
                    BuildConfig.ADDCEL_APP_ID,
                    4,
                    StringUtil.getCurrentLanguage(),
                    it[0].codigo
                )
            }
            .flatMap {

                provincias.addAll(it)

                catalogo.getDistritos(
                    BuildConfig.ADDCEL_APP_ID,
                    4,
                    StringUtil.getCurrentLanguage(),
                    it[0].codigo
                )
            }.observeOn(AndroidSchedulers.mainThread()).subscribe({
                distritos.addAll(it)

                view.enableEnroll()
                view.hideRetry()
                view.hideProgress()
                view.setInitialValues(deptos, provincias, distritos)
            }, {

                view.disableEnroll()
                view.hideProgress()
                view.showRetry()
                view.setInitialValues(
                    listOf(),
                    listOf(),
                    listOf()
                )
                view.onError(ErrorUtils.getFormattedHttpErrorMsg(it))
            })


    }

    override fun getDeptos(): Disposable {
        view.showProgress()
        view.disableEnroll()
        return catalogo.getDepartamentos(
            BuildConfig.ADDCEL_APP_ID,
            4,
            StringUtil.getCurrentLanguage()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view.hideProgress()
                    view.enableEnroll()
                    view.setDeptos(it)
                }, {
                    view.hideProgress()
                    view.enableEnroll()
                    view.setDeptos(listOf())
                    view.onError(ErrorUtils.getFormattedHttpErrorMsg(it))
                }
            )
    }

    override fun getProvincia(depto: DeptoEntity, currDeptoPos: Int): Disposable {
        view.showProgress()
        view.disableEnroll()
        return catalogo.getProvincias(
            BuildConfig.ADDCEL_APP_ID,
            4,
            StringUtil.getCurrentLanguage(), depto.codigo
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view.hideProgress()
                    view.enableEnroll()
                    view.setProvincias(it, currDeptoPos)
                }, {
                    view.hideProgress()
                    view.enableEnroll()
                    view.setProvincias(listOf(), currDeptoPos)
                    view.onError(ErrorUtils.getFormattedHttpErrorMsg(it))
                }
            )
    }

    override fun getDistrito(
        provincia: ProvinciaEntity,
        currProvinciaPos: Int
    ): Disposable {
        return catalogo.getDistritos(
            BuildConfig.ADDCEL_APP_ID,
            4,
            StringUtil.getCurrentLanguage(), provincia.codigo
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view.hideProgress()
                    view.enableEnroll()
                    view.setDistritos(it, currProvinciaPos)
                }, {
                    view.hideProgress()
                    view.enableEnroll()
                    view.setDistritos(listOf(), currProvinciaPos)
                    view.onError(ErrorUtils.getFormattedHttpErrorMsg(it))
                }
            )
    }

    override fun enrollCard(body: TebcaAltaRequest): Disposable {
        view.showProgress()
        view.disableEnroll()
        return api.enrollCard(
            BuildConfig.ADDCEL_APP_ID,
            4,
            StringUtil.getCurrentLanguage(),
            TppAPI.TIPO_OPERADOR,
            repository.read()!!.id,
            body
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                view.hideProgress()
                view.enableEnroll()
                view.onEnroll(it)
            }, {
                view.hideProgress()
                view.enableEnroll()
                view.onError(ErrorUtils.getFormattedHttpErrorMsg(it))
            })
    }

    override fun addToDisposables(disposable: Disposable) {

    }

    override fun clearDisposables() {

    }

}