package mx.mobilecard.tpp.ui.historial.recargas

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import mx.mobilecard.tpp.BuildConfig
import mx.mobilecard.tpp.data.local.user.UserRepository
import mx.mobilecard.tpp.data.network.tpp.TppAPI
import mx.mobilecard.tpp.ui.BasePresenter
import mx.mobilecard.tpp.utils.ErrorUtils
import mx.mobilecard.tpp.utils.StringUtil

/**
 * ADDCEL on 2019-11-27.
 */
interface HistorialRecargasPresenter : BasePresenter {
    fun getRecargas()
}

class HistorialRecargasPresenterImpl(
    val tppAPI: TppAPI,
    val repository: UserRepository,
    val compositeDisposable: CompositeDisposable = CompositeDisposable(),
    val view: HistorialRecargasView
) : HistorialRecargasPresenter {
    override fun getRecargas() {
        val user = repository.read()
        if (user != null) {
            val aDisp = tppAPI.recargas(
                BuildConfig.ADDCEL_APP_ID,
                4,
                StringUtil.getCurrentLanguage(),
                TppAPI.TIPO_OPERADOR, user.id
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.setRecargas(it.data)
                },
                    {
                        view.onError(ErrorUtils.getFormattedHttpErrorMsg(it))
                    })
            compositeDisposable.add(aDisp)
        }
    }

    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

}