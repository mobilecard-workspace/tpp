package mx.mobilecard.tpp.ui.perfil

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_perfil.*
import mx.mobilecard.tpp.BuildConfig
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.TppApp
import mx.mobilecard.tpp.data.network.tpp.LoginEntity
import mx.mobilecard.tpp.data.network.tpp.TppAPI
import mx.mobilecard.tpp.ui.listener.NumberKeyboardListener
import mx.mobilecard.tpp.utils.AndroidUtils
import mx.mobilecard.tpp.utils.StringUtil


class PerfilActivity : AppCompatActivity() {


    companion object {
        fun get(context: Context): Intent {
            return Intent(context, PerfilActivity::class.java)
        }
    }

    val api = TppAPI.get(TppApp.get().retrofit)
    val repository = TppApp.get().userRepository
    val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perfil)
        loadData()
    }


    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    fun loadData() {
        val user = repository.read()
        if (user != null) {
            progress_perfil.visibility = View.VISIBLE
            compositeDisposable.add(
                api.perfil(
                    BuildConfig.ADDCEL_APP_ID,
                    4,
                    StringUtil.getCurrentLanguage(),
                    TppAPI.TIPO_OPERADOR,
                    user.id
                ).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        progress_perfil.visibility = View.GONE
                        if (it.idError == 0) {
                            setData(it)
                            repository.update(it)
                        } else {
                            setData(user)
                        }
                    }, {
                        progress_perfil.visibility = View.GONE
                        setData(user)
                    })
            )
        }
    }

    fun setData(user: LoginEntity) {
        text_perfil_nombre.text =
            AndroidUtils.buildMultiColorString(
                this,
                "Nombre:",
                user.nombre,
                R.color.colorAccent,
                R.color.colorFontLight,
                "\n"
            )

        val saldo = NumberKeyboardListener.getFormattedMontoFromDouble(user.card.saldo, 4)

        text_perfil_saldo.text =
            AndroidUtils.buildMultiColorString(
                this,
                "Saldo:",
                saldo,
                R.color.colorAccent,
                R.color.colorFontLight,
                "\n"
            )

    }
}
