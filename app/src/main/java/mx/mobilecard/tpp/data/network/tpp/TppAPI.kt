package mx.mobilecard.tpp.data.network.tpp

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.http.*

/**
 * ADDCEL on 2019-11-25.
 */
interface TppAPI {

    companion object {

        const val TIPO_OPERADOR = "operador"
        private const val AUTH = "Authorization: Basic TWIxbDNDNHJkbVg6MUxvMGZRI3hHVHN1MXojNFs0TCo="

        fun get(retrofit: Retrofit): TppAPI {
            return retrofit.create(TppAPI::class.java)
        }
    }

    @Headers(AUTH)
    @POST("TPP/{idApp}/{idPais}/{idioma}/{tipo}/login")
    fun login(
        @Path("idApp") idApp: Int,
        @Path("idPais") idPais: Int,
        @Path("idioma") idioma: String,
        @Path("tipo") tipo: String,
        @Body request: LoginRequest
    ): Observable<LoginEntity>


    @Headers(AUTH)
    @POST("TPP/{idApp}/{idPais}/{idioma}/{tipo}/{id}/enrollCard")
    fun enrollCard(
        @Path("idApp") idApp: Int,
        @Path("idPais") idPais: Int,
        @Path("idioma") idioma: String,
        @Path("tipo") tipo: String, @Path("id") id: Int, @Body request: TebcaAltaRequest
    ): Observable<TebcaAltaResponse>


    @Headers(AUTH)
    @GET("TPP/{idApp}/{idPais}/{idioma}/{tipo}/{id}/getCards")
    fun getCards(
        @Path("idApp") idApp: Int,
        @Path("idPais") idPais: Int,
        @Path("idioma") idioma: String,
        @Path("tipo") tipo: String, @Path("id") id: Int
    ): Observable<TebcaAltaResponse>


    @Headers(AUTH)
    @Streaming
    @GET("TPP/{idApp}/{idPais}/{idioma}/{tipo}/{id}/getCards")
    fun getCardsStream(
        @Path("idApp") idApp: Int,
        @Path("idPais") idPais: Int,
        @Path("idioma") idioma: String,
        @Path("tipo") tipo: String, @Path("id") id: Int
    ): Observable<ResponseBody>


    @Headers(AUTH)
    @POST("TPP/{idApp}/{idPais}/{idioma}/{tipo}/recarga")
    fun recarga(
        @Path("idApp") idApp: Int,
        @Path("idPais") idPais: Int,
        @Path("idioma") idioma: String,
        @Path("tipo") tipo: String, @Body request: RecargaRequest
    ): Observable<RecargaEntity>


    @Headers(AUTH)
    @GET("TPP/{idApp}/{idPais}/{idioma}/{tipo}/{id}/perfil")
    fun perfil(
        @Path("idApp") idApp: Int,
        @Path("idPais") idPais: Int,
        @Path("idioma") idioma: String,
        @Path("tipo") tipo: String, @Path("id") id: Int
    ): Observable<LoginEntity>


    @Headers(AUTH)
    @GET("TPP/{idApp}/{idPais}/{idioma}/{tipo}/{id}/historial/1")
    fun altas(
        @Path("idApp") idApp: Int,
        @Path("idPais") idPais: Int,
        @Path("idioma") idioma: String,
        @Path("tipo") tipo: String, @Path("id") id: Int
    ): Observable<HistorialAltaResponse>


    @Headers(AUTH)
    @GET("TPP/{idApp}/{idPais}/{idioma}/{tipo}/{id}/historial/2")
    fun recargas(
        @Path("idApp") idApp: Int,
        @Path("idPais") idPais: Int,
        @Path("idioma") idioma: String,
        @Path("tipo") tipo: String, @Path("id") id: Int
    ): Observable<HistorialRecargaResponse>
}