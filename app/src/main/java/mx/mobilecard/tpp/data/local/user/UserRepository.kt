package mx.mobilecard.tpp.data.local.user

import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.gson.Gson
import mx.mobilecard.tpp.data.network.tpp.LoginEntity

/**
 * ADDCEL on 2019-11-25.
 */
interface UserRepository {
    fun create(user: LoginEntity)
    fun read(): LoginEntity?
    fun update(user: LoginEntity)
    fun delete()
}

class UserRepositoryImpl(val prefs: SharedPreferences, val gson: Gson) : UserRepository {

    companion object {
        private val KEY_USER = "user"
    }

    override fun create(user: LoginEntity) {
        prefs.edit {
            putString(KEY_USER, gson.toJson(user))
        }
    }

    override fun read(): LoginEntity? {
        return if (prefs.contains(KEY_USER)) {
            gson.fromJson(prefs.getString(KEY_USER, ""), LoginEntity::class.java)
        } else {
            null
        }
    }

    override fun update(user: LoginEntity) {
        if (prefs.contains(KEY_USER)) {
            prefs.edit {
                putString(KEY_USER, gson.toJson(user))
            }
        } else {
            throw IllegalStateException("Sesion no iniciada")
        }
    }

    override fun delete() {
        if (prefs.contains(KEY_USER)) {
            prefs.edit {
                remove(KEY_USER)
            }
        } else {
            throw IllegalStateException("Sesion no iniciada")
        }
    }

}