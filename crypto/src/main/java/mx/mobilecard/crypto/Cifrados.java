package mx.mobilecard.crypto;

public enum Cifrados {
    LIMPIO, HARD, SENSITIVE, MIXED, MOBILECARD, CLEAN_RESPONSE
}
