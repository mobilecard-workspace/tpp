package mx.mobilecard.crypto;

import java.security.MessageDigest;

class Crypto {
    public static String md5(String s) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte passDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexPass = new StringBuilder();
            for (byte aPassDigest : passDigest) {
                StringBuilder h = new StringBuilder(Integer.toHexString(0xFF & aPassDigest));
                while (h.length() < 2) {
                    h.insert(0, "0");
                }

                hexPass.append(h);
            }

            return hexPass.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String sha1(String s) {
        StringBuilder sha1 = new StringBuilder();

        try {
            MessageDigest digester = MessageDigest.getInstance("SHA-1");
            digester.update(s.getBytes());
            byte[] digest = digester.digest();

            for (byte b : digest) {
                sha1.append(String.format("%02x", b & 0xff));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sha1.toString();
    }

    public static String aesEncrypt(String seed, String cleartext) {
        try {
            return AES.encode(replaceConAcento(cleartext), seed);
        } catch (Exception e) {
            System.out.println("Exception " + e.getMessage());
            return "";
        }
    }

    public static String aesDecrypt(String seed, String encrypted) {
        try {
            return reemplazaHTML(AES.decode(encrypted, seed));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static String replaceConAcento(String text) {
        StringBuilder sBuffer = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == 'Ñ') {
                sBuffer.append("&Ntilde;");
            } else if (text.charAt(i) == 'ñ') {
                sBuffer.append("&ntilde;");
            } else if (text.charAt(i) == 'Á') {
                sBuffer.append("&Aacute;");
            } else if (text.charAt(i) == 'á') {
                sBuffer.append("&aacute;");
            } else if (text.charAt(i) == 'É') {
                sBuffer.append("&Eacute;");
            } else if (text.charAt(i) == 'é') {
                sBuffer.append("&eacute;");
            } else if (text.charAt(i) == 'Í') {
                sBuffer.append("&Iacute;");
            } else if (text.charAt(i) == 'í') {
                sBuffer.append("&iacute;");
            } else if (text.charAt(i) == 'Ó') {
                sBuffer.append("&Oacute;");
            } else if (text.charAt(i) == 'ó') {
                sBuffer.append("&oacute;");
            } else if (text.charAt(i) == 'Ú') {
                sBuffer.append("&Uacute;");
            } else if (text.charAt(i) == 'ú') {
                sBuffer.append("&uacute;");
            } else {
                sBuffer.append(text.charAt(i));
            }
        }

        return sBuffer.toString();
    }

    private static String reemplazaHTML(String html) {
        final String caracteres[] = {
                "&Aacute;", "&aacute;", "&Eacute;", "&eacute;", "&Iacute;", "&iacute;", "&Oacute;",
                "&oacute;", "&Uacute;", "&uacute;", "&Ntilde;", "&ntilde;"
        };
        final String letras[] = {"Á", "á", "É", "é", "Í", "í", "Ó", "ó", "Ú", "ú", "Ñ", "ñ"};

        for (int counter = 0; counter < caracteres.length; counter++) {
            html = reemplaza(html, caracteres[counter], letras[counter]);
        }
        return html;
    }

    private static String reemplaza(String src, String orig, String nuevo) {
        if (src == null) {
            return null;
        }
        int tmp = 0;
        StringBuilder srcnew = new StringBuilder();
        while (tmp >= 0) {
            tmp = src.indexOf(orig);
            if (tmp >= 0) {
                if (tmp > 0) {
                    srcnew.append(src, 0, tmp);
                }
                srcnew.append(nuevo);
                src = src.substring(tmp + orig.length());
            }
        }
        srcnew.append(src);
        return srcnew.toString();
    }
}