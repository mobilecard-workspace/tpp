package mx.mobilecard.tpp.ui.recarga.seleccion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.mobilecard.crypto.AddcelCrypto
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.data.network.tpp.TebcaCardEntity
import mx.mobilecard.tpp.utils.StringUtil

/**
 * ADDCEL on 2019-11-26.
 */
class RecargaSeleccionAdapter(val items: List<TebcaCardEntity>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> {
                val v =
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_recarga_seleccion, parent, false)
                ItemViewHolder(v)
            }
            TYPE_HEADER -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.header_recarga_seleccion, parent, false)
                HeaderViewHolder(v)
            }
            else -> throw RuntimeException(
                "there is no type that matches the type $viewType + make sure your using types correctly"
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            val model = getItem(position)
            holder.name.text = model.nombre
            holder.card.text = StringUtil.maskCard(AddcelCrypto.decryptHard(model.pan))
        } else if (holder is HeaderViewHolder) {
            holder.header.text = "Selecciona tarjeta"
        }
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (holder is HeaderViewHolder || payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            val o = payloads[0] as Bundle
            var nombre: String? = ""
            var tarjeta: String? = ""
            for (key in o.keySet()) {
                if (key == "nombre") {
                    nombre = o.getString(key)
                    (holder as ItemViewHolder).name.text = nombre
                }
                if (key == "tarjeta") {
                    tarjeta = o.getString(key)
                    (holder as ItemViewHolder).card.text = AddcelCrypto.decryptHard(tarjeta)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (isPositionHeader(position)) TYPE_HEADER else TYPE_ITEM
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    fun getItem(pos: Int): TebcaCardEntity {
        return items[pos - 1]
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val card: TextView = view.findViewById(R.id.text_card_pan)
        val name: TextView = view.findViewById(R.id.text_card_name)
    }

    class HeaderViewHolder(view: View) :
        RecyclerView.ViewHolder(view) {
        val header: TextView = view.findViewById(R.id.text_header)
    }
}