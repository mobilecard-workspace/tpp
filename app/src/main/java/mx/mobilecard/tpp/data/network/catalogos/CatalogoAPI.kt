package mx.mobilecard.tpp.data.network.catalogos

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * ADDCEL on 21/11/19.
 */
interface CatalogoAPI {

    companion object {
        fun get(retrofit: Retrofit): CatalogoAPI {
            return retrofit.create(CatalogoAPI::class.java)
        }
    }

    @GET("Catalogos/{idApp}/{idPais}/{idioma}/getDepartamentos")
    fun getDepartamentos(
        @Path("idApp")
        idApp: Int, @Path("idPais") idPais: Int, @Path("idioma")
        idioma: String
    ): Observable<List<DeptoEntity>>

    @GET("Catalogos/{idApp}/{idPais}/{idioma}/getProvincias")
    fun getProvincias(
        @Path("idApp")
        idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Query("codigo")
        codigo: String
    ): Observable<List<ProvinciaEntity>>

    @GET("Catalogos/{idApp}/{idPais}/{idioma}/getDistritos")
    fun getDistritos(
        @Path("idApp")
        idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Query("codigo")
        codigo: String
    ): Observable<List<DistritoEntity>>

}