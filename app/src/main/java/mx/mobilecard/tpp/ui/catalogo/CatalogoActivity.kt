package mx.mobilecard.tpp.ui.catalogo

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_catalogo.*
import mx.mobilecard.tpp.BuildConfig
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.TppApp
import mx.mobilecard.tpp.data.network.catalogos.CatalogoAPI
import mx.mobilecard.tpp.data.network.catalogos.DeptoEntity
import mx.mobilecard.tpp.data.network.catalogos.ProvinciaEntity
import mx.mobilecard.tpp.ui.RetryView
import mx.mobilecard.tpp.ui.utils.ItemClickSupport
import mx.mobilecard.tpp.utils.StringUtil


enum class Subdivision {
    DEPTO, PROVINCIA, DISTRITO
}

@Parcelize
data class CatalogoViewModel(
    val subdivision: Subdivision,
    val depto: DeptoEntity? = null,
    val provinciaEntity: ProvinciaEntity? = null
) : Parcelable

class CatalogoActivity : AppCompatActivity(), RetryView {

    companion object {
        const val REQUEST_CODE = 100
        const val RESULT_SUBDIVISION = "mx.mobilecard.tpp.ui.catalogo.SUBDIVISION"
        const val RESULT_DATA = "mx.mobilecard.tpp.ui.catalogo.DATA"


        fun startForResult(
            context: Context,
            viewModel: CatalogoViewModel = CatalogoViewModel(Subdivision.DEPTO),
            requestCode: Int
        ) {
            val intent = Intent(context, CatalogoActivity::class.java).putExtra("model", viewModel)
            (context as Activity).startActivityForResult(intent, requestCode)
        }
    }

    private lateinit var viewModel: CatalogoViewModel
    private lateinit var deptoAdapter: DeptoAdapter
    private lateinit var provinciaAdapter: ProvinciaAdapter
    private lateinit var distritoAdapter: DistritoAdapter

    private val retrofit = TppApp.get().retrofit
    private val catalogo = CatalogoAPI.get(retrofit)
    private val disposables = CompositeDisposable()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catalogo)
        viewModel = intent?.getParcelableExtra("model")!!
        configAdapter()
        configRecyclerView(viewModel.subdivision)
        getCatalogo()
    }

    override fun configRetry(view: View) {

    }

    override fun showRetry() {

    }

    override fun hideRetry() {

    }

    override fun clickRetry() {

    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun onError(msg: String) {

    }

    override fun onSuccess(msg: String) {

    }

    private fun configAdapter() {
        when (viewModel.subdivision) {
            Subdivision.DEPTO -> deptoAdapter = DeptoAdapter()
            Subdivision.PROVINCIA -> provinciaAdapter = ProvinciaAdapter()
            Subdivision.DISTRITO -> distritoAdapter = DistritoAdapter()
        }
    }

    private fun configRecyclerView(subdivision: Subdivision) {
        when (subdivision) {
            Subdivision.DEPTO -> recycler_catalogo.adapter = deptoAdapter
            Subdivision.PROVINCIA -> recycler_catalogo.adapter = provinciaAdapter
            Subdivision.DISTRITO -> recycler_catalogo.adapter = distritoAdapter
        }
        recycler_catalogo.layoutManager = LinearLayoutManager(this)
        recycler_catalogo.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )
        ItemClickSupport.addTo(recycler_catalogo).setOnItemClickListener { _, pos, _ ->
            onCatalogoClick(viewModel.subdivision, pos)
        }
    }

    private fun getCatalogo() {
        when (viewModel.subdivision) {
            Subdivision.DEPTO -> disposables.add(getDeptos())
            Subdivision.PROVINCIA -> disposables.add(getProvincias(viewModel.depto!!))
            Subdivision.DISTRITO -> disposables.add(getDistritos(viewModel.provinciaEntity!!))
        }
    }

    private fun getDeptos(): Disposable {
        return catalogo.getDepartamentos(
            BuildConfig.ADDCEL_APP_ID, 4, StringUtil.getCurrentLanguage()
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                deptoAdapter.update(it)
            }, {
                deptoAdapter.update(listOf())
            })
    }

    private fun getProvincias(d: DeptoEntity): Disposable {
        return catalogo.getProvincias(
            BuildConfig.ADDCEL_APP_ID,
            4,
            StringUtil.getCurrentLanguage(),
            d.codigo
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    provinciaAdapter.update(it)
                },
                {
                    provinciaAdapter.update(listOf())
                }
            )
    }

    private fun getDistritos(p: ProvinciaEntity): Disposable {
        return catalogo.getDistritos(
            BuildConfig.ADDCEL_APP_ID,
            4,
            StringUtil.getCurrentLanguage(),
            p.codigo
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    distritoAdapter.update(it)
                },
                {
                    distritoAdapter.update(listOf())
                }
            )
    }

    private fun onCatalogoClick(subdivision: Subdivision, pos: Int) {
        when (subdivision) {
            Subdivision.DEPTO -> onDeptoClick(pos)
            Subdivision.PROVINCIA -> onProvinciaClick(pos)
            Subdivision.DISTRITO -> onDistritoClick(pos)
        }
    }

    private fun onDeptoClick(pos: Int) {
        val intent = Intent()
            .putExtra(RESULT_SUBDIVISION, Subdivision.DEPTO)
            .putExtra(RESULT_DATA, deptoAdapter.getItem(pos))
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun onProvinciaClick(pos: Int) {
        val intent = Intent()
            .putExtra(RESULT_SUBDIVISION, Subdivision.PROVINCIA)
            .putExtra(RESULT_DATA, provinciaAdapter.getItem(pos))
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun onDistritoClick(pos: Int) {
        val intent = Intent()
            .putExtra(RESULT_SUBDIVISION, Subdivision.DISTRITO)
            .putExtra(RESULT_DATA, distritoAdapter.getItem(pos))
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }
}
