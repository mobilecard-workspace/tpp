package mx.mobilecard.tpp.ui.recarga.seleccion

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding2.widget.textChanges
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_recarga_seleccion.*
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.TppApp
import mx.mobilecard.tpp.data.network.tpp.TebcaCardEntity
import mx.mobilecard.tpp.data.network.tpp.TppAPI
import mx.mobilecard.tpp.ui.BaseView
import mx.mobilecard.tpp.ui.recarga.monto.RecargaMontoActivity
import mx.mobilecard.tpp.ui.utils.ItemClickSupport
import timber.log.Timber
import java.util.concurrent.TimeUnit


interface RecargaSeleccionView : BaseView {

    fun addEntity(tebcaCardEntity: TebcaCardEntity)

    fun updateEntities(tebcaCardEntities: List<TebcaCardEntity>)

    fun onLoadFinished()

    fun setCards(cards: List<TebcaCardEntity>)

    fun clickCard(card: TebcaCardEntity)
}


class RecargaSeleccionActivity : AppCompatActivity(), RecargaSeleccionView {

    companion object {
        fun get(context: Context): Intent {
            return Intent(context, RecargaSeleccionActivity::class.java)
        }
    }

    val api = TppAPI.get(TppApp.get().retrofit)
    private val repo = TppApp.get().userRepository

    lateinit var adapter: RecargaSeleccionAdapter
    lateinit var viewModel: RecargaSeleccionViewModel
    lateinit var presenter: RecargaSeleccionPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recarga_seleccion)
        presenter = RecargaSeleccionPresenterImpl(api = api, repository = repo, view = this)

        setSupportActionBar(toolbar_recarga_seleccion)

        viewModel = ViewModelProviders.of(this).get(RecargaSeleccionViewModel::class.java)
        adapter = RecargaSeleccionAdapter(viewModel.oldFilteredCards)

        recycler_recarga_seleccion.adapter = adapter
        recycler_recarga_seleccion.layoutManager = LinearLayoutManager(this)
        recycler_recarga_seleccion.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )


        text_seleccion_search
            .textChanges()
            .debounce(200, TimeUnit.MILLISECONDS)
            .subscribe {
                viewModel
                    .search(it.toString())
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        val diffResult = DiffUtil.calculateDiff(
                            CardiffUtilCallback(
                                viewModel.oldFilteredCards,
                                viewModel.filteredCards
                            )
                        )
                        viewModel.oldFilteredCards.clear()
                        viewModel.oldFilteredCards.addAll(viewModel.filteredCards)
                        diffResult.dispatchUpdatesTo(recycler_recarga_seleccion.adapter as RecyclerView.Adapter<*>)
                    }.addTo(presenter.compositeDisposable)
            }.addTo(presenter.compositeDisposable)


        ItemClickSupport.addTo(recycler_recarga_seleccion)
            .setOnItemClickListener { rv, position, v ->
                val vh = rv.getChildViewHolder(v)
                if (vh is RecargaSeleccionAdapter.ItemViewHolder) {
                    val card = adapter.getItem(position)
                    clickCard(card)
                }
            }
        presenter.getCards()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun addEntity(tebcaCardEntity: TebcaCardEntity) {

    }

    override fun updateEntities(tebcaCardEntities: List<TebcaCardEntity>) {

    }

    override fun onLoadFinished() {

    }

    override fun setCards(cards: List<TebcaCardEntity>) {
        if (cards.isNotEmpty()) {
            viewModel.originalCards.addAll(cards)
            viewModel.oldFilteredCards.addAll(viewModel.originalCards)
            adapter.notifyDataSetChanged()
        } else {
            onError("No hay tarjetas registradas")
            Handler().postDelayed({
                this@RecargaSeleccionActivity.finish()
            }, 250)
        }
    }

    override fun clickCard(card: TebcaCardEntity) {
        Timber.d("Tarjeta a recargar: %s", card)
        startActivity(RecargaMontoActivity.get(this, card))
    }

    override fun showProgress() {
        progress_recarga_seleccion.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_recarga_seleccion.visibility = View.GONE
    }

    override fun onError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun onSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }
}
