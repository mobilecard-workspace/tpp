package mx.mobilecard.tpp.ui.historial.altas

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.screen_historial.*
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.TppApp
import mx.mobilecard.tpp.data.network.tpp.HistorialAltaEntity
import mx.mobilecard.tpp.data.network.tpp.TppAPI
import mx.mobilecard.tpp.ui.BaseView

/**
 * ADDCEL on 2019-11-27.
 */

interface HistorialAltasView : BaseView {
    fun launchAltas()
    fun setAltas(altas: List<HistorialAltaEntity>)
}


class HistorialAltasFragment : Fragment(), HistorialAltasView {

    val api = TppAPI.get(TppApp.get().retrofit)
    val repository = TppApp.get().userRepository
    val adapter = HistorialAltasAdapter(ArrayList())
    lateinit var presenter: HistorialAltasPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = HistorialAltasPresenterImpl(api, repository, view = this)
        launchAltas()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_historial, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_historial.adapter = adapter
        recycler_historial.layoutManager = LinearLayoutManager(view.context)
        recycler_historial.addItemDecoration(
            DividerItemDecoration(
                view.context,
                DividerItemDecoration.VERTICAL
            )
        )
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun launchAltas() {
        presenter.getAltas()
    }

    override fun setAltas(altas: List<HistorialAltaEntity>) {
        presenter.addToDisposables(adapter.update(altas))
    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun onError(msg: String) {
        activity?.let { Toasty.error(it, msg).show() }
    }

    override fun onSuccess(msg: String) {
        activity?.let { Toasty.success(it, msg).show() }
    }


}