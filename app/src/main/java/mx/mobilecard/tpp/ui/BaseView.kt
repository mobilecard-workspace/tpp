package mx.mobilecard.tpp.ui

import android.view.View
import io.reactivex.disposables.Disposable

/**
 * ADDCEL on 2019-11-25.
 */
interface BaseView {
    fun showProgress()
    fun hideProgress()
    fun onError(msg: String)
    fun onSuccess(msg: String)
}

interface BasePresenter {
    fun addToDisposables(disposable: Disposable)
    fun clearDisposables()
}

interface RetryView : BaseView {
    fun configRetry(view: View)
    fun showRetry()
    fun hideRetry()
    fun clickRetry()
}