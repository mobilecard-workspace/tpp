package mx.mobilecard.crypto;

import org.spongycastle.util.encoders.Base64;

import java.util.Date;

public class AddcelCrypto {

    private static final String keyCat = "MTIzNDU2Nzg5MEFCQ0RFRjAxMjM0NTY3ODlBQkNERUY=";
    private static final String keyCard = "NTUyNTk2MzUxMw==";

    /*

     */
    public static String encryptHard(String json) {
        String respuesta;
        try {
            respuesta = encrypt(new String(Base64.decode(keyCat)), json);
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = null;
        }
        return respuesta;
    }

    public static String encryptSensitive(String json) {
        String respuesta;
        String key = AddcelCrypto.getKey();
        try {
            json = encrypt(parsePass(key).toString(), json);
            respuesta = mergeAsf(json, key);
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = null;
        }
        return respuesta;
    }

    public static String encryptPassword(String json) {
        String respuesta;
        try {
            respuesta = encrypt(parsePass(json).toString(), json);
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = null;
        }
        return respuesta;
    }

    private static String encrypt(String key, String json) {
        String respuesta;
        try {
            respuesta = Crypto.aesEncrypt(key, json);
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = null;
        }
        return respuesta;
    }

    public static String decryptHard(String json) {
        String respuesta;
        try {
            respuesta = decrypt(new String(Base64.decode(keyCat)), json);
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = null;
        }
        return respuesta;
    }

    public static String decryptSensitive(String json) {
        String key;
        String[] cad;
        String respuesta;
        try {
            cad = Asf(json);
            key = cad[0];
            json = cad[1];
            respuesta = decrypt(key, json);
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = null;
        }
        return respuesta;
    }

    public static String decryptCard(String json) {
        String respuesta;
        try {
            respuesta = decrypt(parsePass(new String(Base64.decode(keyCard))).toString(), json);
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = null;
        }
        return respuesta;
    }

    public static String encryptCard(String json) {
        String respuesta;
        try {
            respuesta = encrypt(parsePass(new String(Base64.decode(keyCard))).toString(), json);
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = null;
        }
        return respuesta;
    }

    private static String decrypt(String key, String json) {
        String respuesta;
        try {
            respuesta = Crypto.aesDecrypt(key, json.replace(" ", "+"));
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = null;
        }
        return respuesta;
    }

    private static String[] Asf(String cadena) {

        StringBuffer key = new StringBuffer();
        String cadenaCompleta;
        String[] res = new String[3];
        int Long;
        StringBuilder tempo = new StringBuilder();
        try {

            Long = Integer.parseInt(cadena.substring(0, 2));

            cadenaCompleta = cadena.substring(2);

            if (Long <= 8) {
                key.append(cadena, 21, 23).append(cadena, 25, 27).
                        append(cadena, 29, 31).append(cadena, 33, 35);
                cadenaCompleta = cadenaCompleta.substring(0, 19) + cadenaCompleta.substring(21);
                cadenaCompleta = cadenaCompleta.substring(0, 21) + cadenaCompleta.substring(23);
                cadenaCompleta = cadenaCompleta.substring(0, 23) + cadenaCompleta.substring(25);
                cadenaCompleta = cadenaCompleta.substring(0, 25) + cadenaCompleta.substring(27);
            }
            //			else{
            //				for (int i =0, j = 21; i < ciclos[Long]; i++, j+=4){
            //					key.append(cadena.substring(j,j+2));
            //					cadenaCompleta = cadenaCompleta.substring(i,i+19) + cadenaCompleta.substring(i+21) ;
            //					System.out.printlnebug("i: " + i + "   key: " + key + "    cadena: " + cadenaCompleta);
            //				}
            //			}
            //			System.out.printlnebug(new StringBuffer("Paso 0: ").append( key ));

            if (Long == 9) {
                key.append(cadena, 21, 23).append(cadena, 25, 27).
                        append(cadena, 29, 31).append(cadena, 33, 35).
                        append(cadena, 37, 38);
                cadenaCompleta = cadenaCompleta.substring(0, 19) + cadenaCompleta.substring(21);
                cadenaCompleta = cadenaCompleta.substring(0, 21) + cadenaCompleta.substring(23);
                cadenaCompleta = cadenaCompleta.substring(0, 23) + cadenaCompleta.substring(25);
                cadenaCompleta = cadenaCompleta.substring(0, 25) + cadenaCompleta.substring(27);
                cadenaCompleta = cadenaCompleta.substring(0, 27) + cadenaCompleta.substring(28);
            }
            //			System.out.printlnebug(new StringBuffer("Paso 1: ").append( key ));

            if (Long == 10) {
                key.append(cadena, 21, 23).append(cadena, 25, 27).
                        append(cadena, 29, 31).append(cadena, 33, 35).
                        append(cadena, 37, 39);
                cadenaCompleta = cadenaCompleta.substring(0, 19) + cadenaCompleta.substring(21);
                cadenaCompleta = cadenaCompleta.substring(0, 21) + cadenaCompleta.substring(23);
                cadenaCompleta = cadenaCompleta.substring(0, 23) + cadenaCompleta.substring(25);
                cadenaCompleta = cadenaCompleta.substring(0, 25) + cadenaCompleta.substring(27);
                cadenaCompleta = cadenaCompleta.substring(0, 27) + cadenaCompleta.substring(29);
            }

            //			System.out.printlnebug(new StringBuffer("Paso 2: ").append( key ));
            if (Long == 11) {
                key.append(cadena, 21, 23).append(cadena, 25, 27).
                        append(cadena, 29, 31).append(cadena, 33, 35).
                        append(cadena, 37, 38).append(cadena, 38, 39).
                        append(cadena, 41, 42);
                cadenaCompleta = cadenaCompleta.substring(0, 19) + cadenaCompleta.substring(21);
                cadenaCompleta = cadenaCompleta.substring(0, 21) + cadenaCompleta.substring(23);
                cadenaCompleta = cadenaCompleta.substring(0, 23) + cadenaCompleta.substring(25);
                cadenaCompleta = cadenaCompleta.substring(0, 25) + cadenaCompleta.substring(27);
                cadenaCompleta = cadenaCompleta.substring(0, 27) + cadenaCompleta.substring(29);
                cadenaCompleta = cadenaCompleta.substring(0, 29) + cadenaCompleta.substring(30);
            }
            //			System.out.printlnebug(new StringBuffer("Paso 3: ").append( key ));

            if (Long == 12) {
                key.append(cadena, 21, 23).append(cadena, 25, 27).
                        append(cadena, 29, 31).append(cadena, 33, 35).
                        append(cadena, 37, 38).append(cadena, 38, 39).
                        append(cadena, 41, 42).append(cadena, 42, 43);

                cadenaCompleta = cadenaCompleta.substring(0, 19) + cadenaCompleta.substring(21);
                cadenaCompleta = cadenaCompleta.substring(0, 21) + cadenaCompleta.substring(23);
                cadenaCompleta = cadenaCompleta.substring(0, 23) + cadenaCompleta.substring(25);
                cadenaCompleta = cadenaCompleta.substring(0, 25) + cadenaCompleta.substring(27);
                cadenaCompleta = cadenaCompleta.substring(0, 27) + cadenaCompleta.substring(29);
                cadenaCompleta = cadenaCompleta.substring(0, 29) + cadenaCompleta.substring(31);
            }

            for (int y = Long - 1; y != -1; y--) {
                tempo.append(key.substring(y, y + 1));
            }

            res[2] = key.toString();
            key = parsePass(tempo.toString());
            res[0] = key.toString();
            res[1] = cadenaCompleta;
        } catch (Exception e) {
            e.printStackTrace();
            res = null;
        }
        return res;
    }

    private static StringBuffer parsePass(String pass) {
        int len = pass.length();
        StringBuffer key = new StringBuffer();

        for (int i = 0; i < 32 / len; i++) {
            key.append(pass);
        }

        int carry = 0;
        while (key.length() < 32) {
            key.append(pass.charAt(carry));
            carry++;
        }
        return key;
    }

    private static String mergeAsf(String json, String key) {
        StringBuilder result = new StringBuilder();
        StringBuilder builder;
        int offset;
        String next;
        String sub1;
        String sub2;
        try {
            builder = new StringBuilder(key);
            key = builder.reverse().toString();

            result.append(
                    (Integer.toString(key.length()).length() < 2) ? "0" + key.length() : key.length());

            sub1 = json.substring(0, 19);
            sub2 = json.substring(19);

            result.append(sub1);

            for (int i = 0; i < key.length(); i += 2) {
                offset = ((i + 2) <= key.length()) ? (i + 2) : (i + 1);
                next = key.substring(i, offset);

                result.append(next);
                result.append(sub2, 0, 2);
                sub2 = sub2.substring(2);
            }
            result.append(sub2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    public static String mergeStr(String first, String second) {
        StringBuilder builder;
        StringBuilder result = new StringBuilder();
        builder = new StringBuilder(second);
        String other = builder.reverse().toString();

        result.append((Integer.toString(other.length()).length() < 2) ? "0" + other.length()
                : Integer.toString(other.length()));

        String sub1 = first.substring(0, 19);
        String sub2 = first.substring(19);

        result.append(sub1);

        for (int i = 0; i < other.length(); i += 2) {
            int offset = ((i + 2) <= other.length()) ? (i + 2) : (i + 1);
            String next = other.substring(i, offset);

            result.append(next);

            result.append(sub2, 0, 2);

            sub2 = sub2.substring(2);

            //			System.out.printlnebug("i: " + i + "  offset: " + offset + "   result: " + result + "    next: " + next + "   sub2: " + sub2);
        }
        result.append(sub2);

        return result.toString();
    }

    private static String getKey() {
        Long seed = new Date().getTime();

        return String.valueOf(seed).substring(0, 8);
    }
}
