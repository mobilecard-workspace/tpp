package mx.mobilecard.tpp.ui.recarga.monto

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_monto_keyboard.*
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.TppApp
import mx.mobilecard.tpp.data.network.tpp.RecargaEntity
import mx.mobilecard.tpp.data.network.tpp.TebcaCardEntity
import mx.mobilecard.tpp.data.network.tpp.TppAPI
import mx.mobilecard.tpp.ui.BaseView
import mx.mobilecard.tpp.ui.listener.NumberKeyboardListener
import mx.mobilecard.tpp.ui.recarga.result.RecargaResultActivity
import mx.mobilecard.tpp.utils.StringUtil


@Parcelize
data class RecargaMontoViewModel(val cardEntity: TebcaCardEntity) : Parcelable

interface RecargaMontoView : BaseView, NumberKeyboardListener {
    fun clickRecarga()

    fun onRecarga(result: RecargaEntity)
}

class RecargaMontoActivity : AppCompatActivity(), RecargaMontoView {


    companion object {
        fun get(context: Context, cardEntity: TebcaCardEntity): Intent {
            return Intent(context, RecargaMontoActivity::class.java).putExtra(
                "model",
                RecargaMontoViewModel(cardEntity)
            )
        }
    }


    val api = TppAPI.get(TppApp.get().retrofit)
    val repository = TppApp.get().userRepository

    private var montoString = ""
    lateinit var viewModel: RecargaMontoViewModel
    lateinit var presenter: RecargaMontoPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monto_keyboard)

        viewModel = intent?.getParcelableExtra("model")!!
        presenter = RecargaMontoPresenterImpl(api = api, repository = repository, view = this)

        text_keyboard_monto.isFocusable = false
        text_keyboard_monto.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(editable: Editable) {
                b_keyboard_total_delete.isEnabled = editable.isNotEmpty()
                b_keyboard_recargar.isEnabled = editable.isNotEmpty()
            }
        })
        b_keyboard_total_delete.isEnabled = false
        b_keyboard_recargar.isEnabled = false

        b_keyboard_total_one.setOnClickListener { onKeyStroke("1") }
        b_keyboard_total_two.setOnClickListener { onKeyStroke("2") }
        b_keyboard_total_three.setOnClickListener { onKeyStroke("3") }
        b_keyboard_total_four.setOnClickListener { onKeyStroke("4") }
        b_keyboard_total_five.setOnClickListener { onKeyStroke("5") }
        b_keyboard_total_six.setOnClickListener { onKeyStroke("6") }
        b_keyboard_total_seven.setOnClickListener { onKeyStroke("7") }
        b_keyboard_total_eight.setOnClickListener { onKeyStroke("8") }
        b_keyboard_total_nine.setOnClickListener { onKeyStroke("9") }
        b_keyboard_total_delete.setOnClickListener { onDeleteStroke() }
        b_keyboard_total_zero.setOnClickListener { onKeyStroke("0") }
        b_keyboard_recargar.setOnClickListener { clickRecarga() }


    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun clickRecarga() {
        if (montoString.isNotEmpty()) {
            val monto = montoString.toDouble() / 100
            //val comision = monto * .05
            presenter.recarga(monto, viewModel.cardEntity.id)
        }
    }

    override fun onRecarga(result: RecargaEntity) {
        startActivity(RecargaResultActivity.get(this, result))
    }

    override fun showProgress() {
        progress_keyboard_monto.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_keyboard_monto.visibility = View.GONE
    }

    override fun onError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun onSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun onKeyStroke(captured: String) {
        if (captured.matches(NumberKeyboardListener.PATTERN_DECIMAL.toRegex())) {
            montoString += captured
            text_keyboard_monto.text =
                NumberKeyboardListener.getFormattedMonto(montoString, 4)
        } else {
            montoString = ""
            text_keyboard_monto.text = ""
        }
    }

    override fun onDeleteStroke() {
        montoString = StringUtil.removeLast(montoString)
        text_keyboard_monto.text =
            NumberKeyboardListener.getFormattedMonto(montoString, 4)
    }
}
