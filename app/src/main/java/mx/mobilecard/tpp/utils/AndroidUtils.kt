package mx.mobilecard.tpp.utils

import android.app.Activity
import android.content.Context
import android.location.LocationManager
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.core.util.Preconditions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.material.textfield.TextInputLayout


/**
 * ADDCEL on 21/11/19.
 */
object AndroidUtils {


    @Synchronized
    @Throws(NullPointerException::class)
    fun isLocationAvailable(context: Context): Boolean {
        val manager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        if (manager == null) {
            throw NullPointerException("Device does not support location")
        } else {
            val gpsEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            val networkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

            return gpsEnabled || networkEnabled
        }
    }

    @Synchronized
    fun isGooglePlayServicesAvailable(
        activity: Activity, instance: GoogleApiAvailability
    ): Boolean {
        val status = instance.isGooglePlayServicesAvailable(activity)
        if (status != ConnectionResult.SUCCESS) {
            if (instance.isUserResolvableError(status)) {
                instance.getErrorDialog(activity, status, 2404).show()
            }
            return java.lang.Boolean.FALSE
        }
        return java.lang.Boolean.TRUE
    }

    fun getText(view: TextView?): String {
        return Preconditions.checkNotNull(view).text.toString().trim()
    }

    fun setText(view: TextView?, resId: Int) {
        Preconditions.checkNotNull(view).setText(resId)
    }

    fun setText(view: TextView?, text: CharSequence) {
        Preconditions.checkNotNull(view).text = text
    }


    fun setTextEmpty(@Nullable view: TextView) {
        setText(view, "")
    }

    fun getEditText(til: TextInputLayout): EditText {
        return Preconditions.checkNotNull(til.editText)
    }

    fun buildMultiColorString(
        context: Context,
        key: String,
        value: String,
        colorLeft: Int,
        colorRight: Int,
        delimiter: String = " "
    ): CharSequence {
        val firstPart = SpannableString(key)
        val lastPart = SpannableString(value)

        firstPart.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, colorLeft)), 0,
            firstPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        lastPart.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, colorRight)), 0,
            lastPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        return TextUtils.concat(firstPart, delimiter, lastPart)
    }

}