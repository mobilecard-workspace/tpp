package mx.mobilecard.tpp

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import mx.mobilecard.tpp.ui.login.LoginActivity
import mx.mobilecard.tpp.ui.menu.MenuActivity
import java.util.concurrent.TimeUnit


interface MainView {

    fun checkUser()

    fun onLogged()

    fun onNotLogged()
}

class MainActivity : AppCompatActivity(), MainView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Handler().postDelayed({
            checkUser()
        }, TimeUnit.SECONDS.toMillis(2))
    }

    override fun checkUser() {
        val repo = TppApp.get().userRepository
        if (repo.read() != null) onLogged()
        else onNotLogged()
    }

    override fun onLogged() {
        startActivity(MenuActivity.get(this))
    }

    override fun onNotLogged() {
        startActivity(LoginActivity.get(this))
    }
}
