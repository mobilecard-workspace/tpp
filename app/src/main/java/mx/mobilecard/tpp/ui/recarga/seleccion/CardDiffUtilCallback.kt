package mx.mobilecard.tpp.ui.recarga.seleccion

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import mx.mobilecard.tpp.data.network.tpp.TebcaCardEntity
import java.util.*

/**
 * ADDCEL on 2019-11-26.
 */
class CardiffUtilCallback(
    private val oldList: List<TebcaCardEntity>,
    private val newList: List<TebcaCardEntity>
) : DiffUtil.Callback() {

    private val peLocale = Locale("es", "PE")

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition].id == newList[newItemPosition].id

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem: TebcaCardEntity = oldList[oldItemPosition]
        val newItem: TebcaCardEntity = newList[newItemPosition]
        return oldItem == newItem
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {

        val oldItem: TebcaCardEntity = oldList[oldItemPosition]
        val newItem: TebcaCardEntity = newList[newItemPosition]

        val diff = Bundle()

        if (oldItem.nombre.toUpperCase(peLocale) != newItem.nombre.toUpperCase(peLocale)) {
            diff.putString("nombre", newItem.nombre)
        }

        if (oldItem.pan != newItem.pan) {
            diff.putString("tarjeta", newItem.pan)
        }

        return if (diff.size() == 0) {
            null
        } else diff
    }
}