package mx.mobilecard.tpp.ui.catalogo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.mobilecard.tpp.data.network.catalogos.DeptoEntity
import mx.mobilecard.tpp.data.network.catalogos.DistritoEntity
import mx.mobilecard.tpp.data.network.catalogos.ProvinciaEntity

/**
 * ADDCEL on 2020-01-23.
 */


class DeptoAdapter : RecyclerView.Adapter<DeptoViewHolder>() {
    private val data = mutableListOf<DeptoEntity>()

    fun update(deptos: List<DeptoEntity>) {
        data.clear()
        data.addAll(deptos)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeptoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(android.R.layout.simple_list_item_1, parent, false)
        return DeptoViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: DeptoViewHolder, position: Int) {
        holder.text.text = data[position].toString()
    }

    fun getItem(pos: Int): DeptoEntity {
        return data[pos]
    }

}

class DeptoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val text: TextView = itemView.findViewById(android.R.id.text1)
}


class ProvinciaAdapter : RecyclerView.Adapter<ProvinciaViewHolder>() {
    private val data = mutableListOf<ProvinciaEntity>()

    fun update(provincias: List<ProvinciaEntity>) {
        data.clear()
        data.addAll(provincias)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProvinciaViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(android.R.layout.simple_list_item_1, parent, false)
        return ProvinciaViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ProvinciaViewHolder, position: Int) {
        holder.text.text = data[position].toString()
    }

    fun getItem(pos: Int): ProvinciaEntity {
        return data[pos]
    }

}

class ProvinciaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val text: TextView = itemView.findViewById(android.R.id.text1)
}

class DistritoAdapter : RecyclerView.Adapter<DistritoViewHolder>() {
    private val data = mutableListOf<DistritoEntity>()

    fun update(distritos: List<DistritoEntity>) {
        data.clear()
        data.addAll(distritos)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DistritoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(android.R.layout.simple_list_item_1, parent, false)
        return DistritoViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: DistritoViewHolder, position: Int) {
        holder.text.text = data[position].toString()
    }

    fun getItem(pos: Int): DistritoEntity {
        return data[pos]
    }

}

class DistritoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val text: TextView = itemView.findViewById(android.R.id.text1)
}