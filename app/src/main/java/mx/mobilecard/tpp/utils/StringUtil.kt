package mx.mobilecard.tpp.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.text.TextUtils
import android.util.Base64
import android.util.Patterns
import androidx.core.util.Preconditions
import okio.ByteString.Companion.encodeUtf8
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.util.*
import java.util.regex.Pattern


/**
 * ADDCEL on 2019-11-25.
 */

object StringUtil {

    private val vocales = arrayOf(
        "a", "e", "i", "o", "u"
    )
    private val letras = arrayOf(
        "Á", "á", "É", "é", "Í", "í", "Ó", "ó", "Ú", "ú", "Ñ", "ñ"
    )
    private val reemplazo = arrayOf(
        "A", "a", "E", "e", "I", "i", "O", "o", "U", "u", "N", "n"
    )
    private const val ERROR_ES = "Ocurrió un error"
    private const val ERROR_EN = "An error occured"
    private const val SUCCESS_ES = "Operación completada con éxito"
    private const val SUCCESS_EN = "Operation completed"

    private const val ERROR_NET_ES =
        "No logramos procesar tu petición. Verifica tu conexión a Internet"
    private const val ERROR_NET_EN =
        "Your request cannot be processed at this time. Please check your Internet connection"

    const val TEBCA = "^(?=\\d{16}$)(483039)\\d+"
    const val PREVIVALE = "^(?=\\d{16}$)(506450)\\d+"
    const val PREVIVALE_OR_EMPTY = "(^(?=\\d{16}$)(506450)\\d+)?"
    const val TRANSFER_CLABE = "^\\d{18}$"
    const val TRANSFER_ACCOUNT = "^\\d{10,11}$"
    const val DECIMAL_AMOUNT = "\\d*\\.?\\d+"
    const val MX_ZIP_CODE = "^[0-9]{5}$"
    const val RFC_OR_EMPTY =
        "([A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?)?"
    const val CURP =
        "[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]"
    const val SSN =
        "^(?!219099999|078051120)(?!666|000|9\\d{2})\\d{3}(?!00)\\d{2}(?!0{4})\\d{4}$"

    fun removeAcentos(str: String?): String {
        Preconditions.checkNotNull(str)
        return TextUtils.replace(str, letras, reemplazo).toString()
    }

    fun removeAcentosMultiple(str: String?): String {
        Preconditions.checkNotNull(str)

        val words = TextUtils.split(str, "\\s+")
        val sb = StringBuilder()
        for (s in words) {
            sb.append(removeAcentos(s)).append(" ")
        }
        return sb.toString().trim { it <= ' ' }
    }


    fun findBetween(
        src: String,
        start: String?,
        end: String?
    ): String? {
        return src.substring(src.indexOf(start!!) + 1, src.indexOf(end!!))
    }

    fun amountStringToDouble(amStr: String?): Double {
        return if (amStr.isNullOrEmpty()) 0.0 else amStr.toDouble()
    }

    fun sha256(input: String): String {
        Preconditions.checkNotNull(
            input,
            "Arg a hashear no puede ser NULL"
        )

        return input.encodeUtf8().sha256().hex()
    }

    fun sha512(input: String): String {
        Preconditions.checkNotNull(
            input,
            "Arg a hashear no puede ser NULL"
        )

        return input.encodeUtf8().sha512().base64()
    }

    fun isStringNumberLargerThanValue(amountAsString: String?, compareValue: Double): Boolean {
        val amount =
            if (amountAsString.isNullOrEmpty()) 0.0 else amountAsString.toDouble()
        return amount > compareValue
    }


    /**
     * Tarjeta que sustituye los digitos correspondientes a los indices 4 - 11 con un caracter
     * cualquiera. Si un caracter en cuestion se encuentra antes de un indice multiplo de 4 agrega un
     * espacio a la representacion
     *
     * @param card        - El numero de tarjeta a enmascarar
     * @param placeholder - Caracter a utilizar como sustitucion
     * @return String representando la tarjeta enmascarada
     */
    fun maskCard(card: String, placeholder: String): String {
        Preconditions.checkArgument(
            card.isNotEmpty() && TextUtils.isDigitsOnly(
                card
            )
        )
        val result = java.lang.StringBuilder()
        Timber.d("Longitud tarjeta: %d", card.length)
        for (i in card.indices) {
            if (i in 4..11) {
                if ((i + 1) % 4 == 0) {
                    result.append(placeholder).append(" ")
                } else {
                    result.append(placeholder)
                }
            } else {
                if ((i + 1) % 4 == 0) {
                    result.append(card[i]).append(" ")
                } else {
                    result.append(card[i])
                }
            }
        }
        return result.toString()
    }

    fun maskCard(card: String): String {
        return maskCard(card, "·")
    }

    fun removeLast(str: String): String {
        Preconditions.checkNotNull(str)
        return str.substring(0, str.length - 1)
    }

    fun getCurrentLanguage(): String { //String lang = Locale.getDefault().getLanguage().toLowerCase();
//return Strings.isNullOrEmpty(lang) ? "en" : lang;
        return "es"
    }

    fun byteArrayToBase64(bitmap: Bitmap): String? {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos) // bm is the bitmap object
        val b: ByteArray = baos.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
        // ByteString.of(b).base64();
    }

    fun base64ToByteArray(str: String): Bitmap? {
        val decodedString: ByteArray = Base64.decode(str, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
    }

    fun generarRFC(input: String): String {
        val resultado = java.lang.StringBuilder()
        val nombreSplit = input.split(" ").toTypedArray()
        val nombre = nombreSplit[0]
        val apellidoPaterno = nombreSplit[1]
        val apellidoMaterno = nombreSplit[2]
        var vocalEncontrada = false
        // Sacamos la primera letra del apellido paterno
        resultado.append(apellidoPaterno[0])
        // Sacamos la primera vocal del apellido paterno
        var i = 0
        while (i < apellidoPaterno.length && !vocalEncontrada) {
            val letra = apellidoPaterno[i].toString()
            for (vocal in vocales) {
                if (letra.compareTo(vocal) == 0) {
                    resultado.append(letra)
                    vocalEncontrada = true
                    break
                }
            }
            i++
        }
        // Sacamos la primera letra del apellido materno
        resultado.append(apellidoMaterno[0])
        resultado.append(nombre[0])
        return resultado.toString().toUpperCase(Locale.getDefault())
    }

    fun isMobileCardPlastic(pan: CharSequence): Boolean {
        return Pattern.matches(PREVIVALE, pan)
    }

    fun isMobileCardPlasticREGEX(pan: CharSequence): Boolean {
        val sPan: CharSequence = pan.toString()
        return (Pattern.matches(PREVIVALE, sPan)
                && TextUtils.indexOf(sPan, "506450") == 0)
    }

    fun getUltimos4DigitosTDC(tarjeta: String): String? {
        Preconditions.checkArgument(tarjeta.isNotEmpty() && tarjeta.length >= 4)
        val start = tarjeta.length - 4 // tomando en cuenta index 0
        val end = tarjeta.length
        return TextUtils.substring(tarjeta, start, end)
    }

    fun trimPhone(immutablePhone: String): String? {
        var phone = immutablePhone
        phone = phone.replace("-", "")
        if (phone.length > 10) phone = phone.substring(phone.length - 10)
        return phone
    }

    fun trimPhone(immutablePhone: String, desiredLen: Int): String? {
        var phone = immutablePhone
        phone = phone.replace("-", "")
        if (phone.length > desiredLen) phone = phone.substring(phone.length - desiredLen)
        return phone
    }

    fun trimLoginIfEmail(login: String): String? {
        return if (Patterns.EMAIL_ADDRESS.matcher(login).matches()) TextUtils.split(
            login,
            "@"
        )[0] else login
    }

    fun textOrDefault(text: String, def: String): String {
        return if (text.isEmpty()) def else text
    }

    fun errorOrDefault(error: String, idioma: String): String? {
        return if (idioma == "es") textOrDefault(error, ERROR_ES) else textOrDefault(
            error,
            ERROR_EN
        )
    }

    fun getNetworkError(): String {
        return if (getCurrentLanguage() == "es") ERROR_NET_ES else ERROR_NET_EN
    }

    fun successOrDefault(error: String, idioma: String): String {
        return if (idioma == "es") textOrDefault(error, SUCCESS_ES) else textOrDefault(
            error,
            SUCCESS_EN
        )
    }

    fun fromSequence(sequence: CharSequence?): String? {
        return sequence?.toString()?.trim { it <= ' ' } ?: ""
    }

    fun digest(vararg data: String?): String? {
        val digest = java.lang.StringBuilder()
        for (s in data) {
            digest.append(s)
        }
        return sha512(digest.toString())
    }

    fun splitCard(card: String, divider: String): String {
        val split = java.lang.StringBuilder()
        for (i in card.indices) {
            if (i > 0 && i % 4 == 0) split.append(divider)
            split.append(card[i])
        }
        return split.toString()
    }

    fun parsePlaca(placa: String, divider: String): String {
        if (placa.contains("-")) return placa
        val split = java.lang.StringBuilder()
        for (i in placa.indices) {
            if (i > 0 && i % 3 == 0) split.append(divider)
            split.append(placa[i])
        }
        return split.toString()
    }

    fun lunhCheck(ccNumber: String): Boolean {
        var sum = 0
        var alternate = false
        for (i in ccNumber.length - 1 downTo 0) {
            var n = ccNumber.substring(i, i + 1).toInt()
            if (alternate) {
                n *= 2
                if (n > 9) {
                    n = n % 10 + 1
                }
            }
            sum += n
            alternate = !alternate
        }
        return sum % 10 == 0
    }

}

