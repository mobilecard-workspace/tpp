package mx.mobilecard.tpp.ui.login

import android.util.Patterns
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import mx.mobilecard.tpp.BuildConfig
import mx.mobilecard.tpp.data.local.user.UserRepository
import mx.mobilecard.tpp.data.network.tpp.LoginRequest
import mx.mobilecard.tpp.data.network.tpp.TppAPI
import mx.mobilecard.tpp.ui.BasePresenter
import mx.mobilecard.tpp.utils.ErrorUtils
import mx.mobilecard.tpp.utils.StringUtil

/**
 * ADDCEL on 2019-11-25.
 */
interface LoginPresenter : BasePresenter {

    fun evalForm(login: String, password: String)

    fun login(login: String, password: String)

}

class LoginPresenterImpl(
    private val api: TppAPI,
    private val userRepository: UserRepository,
    private val disposables: CompositeDisposable,
    private val view: LoginView
) : LoginPresenter {

    override fun evalForm(login: String, password: String) {
        if (!Patterns.EMAIL_ADDRESS.matcher(login).matches()) view.onEmailError("Ingresa un correo válido")
        else if (password.isEmpty() || password.length < 8) view.onPasswordError("Ingresa una contraseña válida")
        else view.onFormSuccess()
    }

    override fun login(login: String, password: String) {
        view.showProgress()

        val body = LoginRequest(StringUtil.sha512(password), login)


        val lDisp = api.login(
            BuildConfig.ADDCEL_APP_ID,
            4,
            StringUtil.getCurrentLanguage(),
            TppAPI.TIPO_OPERADOR,
            body
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view.hideProgress()
                    if (it.idError == 0) {
                        userRepository.create(it)
                        view.onLogin(it)
                    } else view.onError(it.mensajeError)
                }, {
                    view.hideProgress()
                    view.onError(ErrorUtils.getFormattedHttpErrorMsg(it))
                }
            )
        addToDisposables(lDisp)
    }

    override fun addToDisposables(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

}