package mx.mobilecard.tpp.ui.historial.recargas

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.mobilecard.crypto.AddcelCrypto
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.data.network.tpp.HistorialRecargaEntity
import mx.mobilecard.tpp.ui.listener.NumberKeyboardListener
import mx.mobilecard.tpp.utils.AndroidUtils
import mx.mobilecard.tpp.utils.StringUtil

/**
 * ADDCEL on 2019-11-27.
 */
class HistorialRecargasAdapter(val items: MutableList<HistorialRecargaEntity>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
    }

    fun update(data: List<HistorialRecargaEntity>) {
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> {
                val v =
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_historial_alta, parent, false)
                ItemViewHolder(v)
            }
            TYPE_HEADER -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.header_recarga_seleccion, parent, false)
                HeaderViewHolder(v)
            }
            else -> throw RuntimeException(
                "there is no type that matches the type $viewType + make sure your using types correctly"
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            val model = getItem(position)

            val name = AndroidUtils.buildMultiColorString(
                holder.text.context,
                "Nombre:", model.nombre.trim(), R.color.colorAccent, R.color.colorFontLight, " "
            )
            val fecha = AndroidUtils.buildMultiColorString(
                holder.text.context,
                "Fecha:", model.fecha, R.color.colorAccent, R.color.colorFontLight, " "
            )
            val tarjeta = AndroidUtils.buildMultiColorString(
                holder.text.context,
                "Tarjeta:",
                StringUtil.maskCard(AddcelCrypto.decryptHard(model.tarjeta)),
                R.color.colorAccent,
                R.color.colorFontLight,
                " "
            )
            val monto = AndroidUtils.buildMultiColorString(
                holder.text.context,
                "Monto:",
                NumberKeyboardListener.getFormattedMontoFromDouble(model.monto, 4),
                R.color.colorAccent,
                R.color.colorFontLight,
                " "
            )


            holder.text.text = TextUtils.concat(name, "\n", fecha, "\n", tarjeta, "\n", monto)
            holder.logo.setImageResource(R.drawable.icon_monedas_naranja)


        } else if (holder is HeaderViewHolder) {
            holder.header.text = "Recargas"
        }
    }

    override fun getItemCount(): Int {
        return items.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (isPositionHeader(position)) TYPE_HEADER else TYPE_ITEM
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    fun getItem(pos: Int): HistorialRecargaEntity {
        return items[pos - 1]
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val logo: ImageView = view.findViewById(R.id.historial_logo)
        val text: TextView = view.findViewById(R.id.historial_text)
    }

    class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val header: TextView = view.findViewById(R.id.text_header)
    }
}
