package mx.mobilecard.tpp.utils

import retrofit2.HttpException

/**
 * ADDCEL on 2019-11-25.
 */


object ErrorUtils {

    const val NETWORK = 1
    const val OPERATION = 2
    const val IMAGE = 3
    const val QR = 4
    const val AUTH = 5
    const val NO_ELEMENTS = 6

    private const val ERROR_ES = "Ocurrió un error. Intente de nuevo más tarde."
    private const val ERROR_EN = "An error occured. Please, try again later."
    private const val ERROR_NET_ES =
        "No logramos procesar tu petición. Verifica tu conexión a Internet."
    private const val ERROR_NET_EN =
        "Your request cannot be processed at this time. Please check your Internet connection."
    private const val ERROR_IMG_ES = "Imagen no cargada. Verifica tu conexión a Internet."
    private const val ERROR_IMG_EN =
        "Image could not be loaded. Please check your Internet connection."
    private const val ERROR_QR_ES =
        "Ocurrió un error en la creación del código QR, intenta de nuevo."
    private const val ERROR_QR_EN = "QR Code could not be created. Please, try again."
    private const val ERROR_AUTH_ES =
        "Ocurrió un error en tu validación biométrica. Intenta de nuevo"
    private const val ERROR_AUTH_EN =
        "Your biometric validation was unsuccessful. Please, try again."
    private const val ERROR_NO_ELEM_ES = "No hay elementos disponibles. Intenta de nuevo"
    private const val ERROR_NO_ELEM_EN = "No content. Please, try again."

    private val enMap = mapOf(
        NETWORK to ERROR_NET_EN, OPERATION to ERROR_EN, IMAGE to ERROR_IMG_EN,
        QR to ERROR_QR_EN, AUTH to ERROR_AUTH_EN, NO_ELEMENTS to ERROR_NO_ELEM_EN
    )

    private val esMap = mapOf(
        NETWORK to ERROR_NET_ES, OPERATION to ERROR_ES, IMAGE to ERROR_IMG_ES,
        QR to ERROR_QR_ES, AUTH to ERROR_AUTH_ES, NO_ELEMENTS to ERROR_NO_ELEM_ES
    )

    fun getErrorMsg(type: Int): String {
        return if (StringUtil.getCurrentLanguage() == "es") {
            esMap.getValue(type)
        } else {
            enMap.getValue(type)
        }
    }

    fun getErrorMsg(type: Int, thr: Throwable, debug: Boolean): String {
        return if (debug) thr.message ?: "DEBUG_ERROR" else getErrorMsg(type)
    }

    fun getFormattedHttpErrorMsg(thr: Throwable): String {
        return if (thr is HttpException) {
            "Error ${thr.code()} -> ${thr.message()}"
        } else {
            thr.localizedMessage ?: getErrorMsg(NETWORK)
        }
    }

    fun getFormattedHttpErrorCode(thr: Throwable): Int {
        return if (thr is HttpException) {
            thr.code()
        } else {
            -9999
        }
    }
}