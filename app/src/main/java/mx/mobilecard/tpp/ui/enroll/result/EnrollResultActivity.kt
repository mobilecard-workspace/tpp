package mx.mobilecard.tpp.ui.enroll.result

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_result.*
import mx.mobilecard.crypto.AddcelCrypto
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.data.network.tpp.TebcaAltaResponse
import mx.mobilecard.tpp.data.network.tpp.TebcaCardEntity
import mx.mobilecard.tpp.ui.menu.MenuActivity
import mx.mobilecard.tpp.ui.recarga.monto.RecargaMontoActivity
import mx.mobilecard.tpp.utils.ScreenshotUtils
import mx.mobilecard.tpp.utils.StringUtil


@Parcelize
data class EnrollResultViewModel(val result: TebcaAltaResponse) : Parcelable

interface EnrollResultView {
    fun setResultImg()

    fun setResultTitle()

    fun setResultDetail()

    fun clickShare()

    fun clickInicio()

    fun showRecarga()

    fun clickRecarga()
}

class EnrollResultActivity : AppCompatActivity(), EnrollResultView {


    companion object {
        fun get(context: Context, result: TebcaAltaResponse): Intent {
            val model = EnrollResultViewModel(result)
            return Intent(context, EnrollResultActivity::class.java).putExtra("model", model)
        }
    }

    lateinit var model: EnrollResultViewModel
    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = intent?.getParcelableExtra("model")!!
        setContentView(R.layout.activity_result)

        setResultImg()
        setResultTitle()
        setResultDetail()
        showRecarga()
        b_result_share.setOnClickListener { clickShare() }
        b_result_inicio.setOnClickListener { clickInicio() }
        b_result_recargar.setOnClickListener { clickRecarga() }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun setResultImg() {
        val img = if (model.result.idError == 0) R.drawable.exito_icon else R.drawable.error_icon
        img_result.setImageResource(img)
    }

    override fun setResultTitle() {
        text_result_title.text =
            if (model.result.idError == 0) "Enrolamiento exitoso" else model.result.mensajeError
    }

    override fun setResultDetail() {
        if (model.result.idError == 0) {
            text_result_detail.text = buildDetail(model.result.cards[0]).toString()
        } else {
            text_result_detail.visibility = View.GONE
        }
    }

    override fun clickShare() {
        compositeDisposable.add(RxPermissions(this).request(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).subscribe {
            if (it) {
                ScreenshotUtils.shareScreenshot(this, "Enrolamiento TPP")
            }
        })

    }

    override fun clickInicio() {
        val intent = MenuActivity.get(this).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    override fun showRecarga() {
        b_result_recargar.visibility = if (model.result.idError == 0) View.VISIBLE else View.GONE
    }

    override fun clickRecarga() {
        startActivity(RecargaMontoActivity.get(this, model.result.cards[0]))
        finish()
    }

    private fun buildDetail(card: TebcaCardEntity): CharSequence {
        return TextUtils.concat(
            getDetailSpan("Nombre:", card.nombre),
            "\n\n",
            getDetailSpan("Fecha:", card.fechaAlta),
            "\n\n",
            getDetailSpan("Tarjeta:", StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan)))
        )
    }

    private fun getDetailSpan(key: String, value: String): CharSequence {
        val firstPart = SpannableString(key)
        val lastPart = SpannableString(value)

        firstPart.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)), 0,
            firstPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        lastPart.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorFontLight)), 0,
            lastPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        return TextUtils.concat(firstPart, " ", lastPart)
    }
}
