package mx.mobilecard.tpp

import android.annotation.SuppressLint
import android.app.Application
import android.util.Log
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.threetenabp.AndroidThreeTen
import mx.mobilecard.tpp.data.local.user.UserRepository
import mx.mobilecard.tpp.data.local.user.UserRepositoryImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber


/**
 * ADDCEL on 19/11/19.
 */
class TppApp : Application() {

    val gson: Gson = GsonBuilder().serializeSpecialFloatingPointValues().create()

    lateinit var userRepository: UserRepository
    lateinit var client: OkHttpClient
    lateinit var retrofit: Retrofit

    companion object {

        private lateinit var instance: TppApp

        fun get(): TppApp {
            return instance
        }
    }

    override fun onCreate() {
        super.onCreate()
        startThreeTenAbp()
        plantTimber()
        buildRepository()
        buildRetrofit()
        instance = this
    }

    private fun startThreeTenAbp() {
        AndroidThreeTen.init(this)
    }

    private fun buildRepository() {
        userRepository =
            UserRepositoryImpl(PreferenceManager.getDefaultSharedPreferences(this), gson)
    }

    private fun plantTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
    }

    private fun buildRetrofit() {
        val builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    Timber.d(message)
                }
            })
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(interceptor)
        }

        client = builder.build()

        retrofit = Retrofit.Builder().client(client)
            .baseUrl(BuildConfig.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    private class CrashReportingTree : Timber.Tree() {
        @SuppressLint("LogNotTimber")
        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) return
            if (t != null) {
                if (priority == Log.ERROR) {
                    t.printStackTrace()
                } else if (priority == Log.WARN) {
                    t.printStackTrace()
                }
            }
        }
    }
}