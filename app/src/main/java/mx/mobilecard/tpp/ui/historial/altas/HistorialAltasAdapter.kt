package mx.mobilecard.tpp.ui.historial.altas

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import mx.mobilecard.crypto.AddcelCrypto
import mx.mobilecard.tpp.R
import mx.mobilecard.tpp.data.network.tpp.HistorialAltaEntity
import mx.mobilecard.tpp.utils.AndroidUtils
import mx.mobilecard.tpp.utils.StringUtil

/**
 * ADDCEL on 2019-11-27.
 */
class HistorialAltasAdapter(private val items: MutableList<HistorialAltaEntity>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
    }

    fun update(data: List<HistorialAltaEntity>): Disposable {

        /*
           final EmployeeDiffCallback diffCallback = new EmployeeDiffCallback(this.mEmployees, employees);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
    
            this.mEmployees.clear();
            this.mEmployees.addAll(employees);
            diffResult.dispatchUpdatesTo(this);
         */

        val callback = HistorialAltasDiffCallback(items, data)

        return Observable.fromCallable {
            DiffUtil.calculateDiff(callback)
        }.subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                items.clear()
                items.addAll(data)
                it.dispatchUpdatesTo(this)
            }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> {
                val v =
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_historial_alta, parent, false)
                ItemViewHolder(v)
            }
            TYPE_HEADER -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.header_recarga_seleccion, parent, false)
                HeaderViewHolder(v)
            }
            else -> throw RuntimeException(
                "there is no type that matches the type $viewType + make sure your using types correctly"
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            val model = getItem(position)

            val name = AndroidUtils.buildMultiColorString(
                holder.text.context,
                "Nombre:", model.nombre.trim(), R.color.colorAccent, R.color.colorFontLight, " "
            )
            val fecha = AndroidUtils.buildMultiColorString(
                holder.text.context,
                "Fecha:", model.fechaAlta, R.color.colorAccent, R.color.colorFontLight, " "
            )
            val tarjeta = AndroidUtils.buildMultiColorString(
                holder.text.context,
                "Tarjeta:",
                StringUtil.maskCard(AddcelCrypto.decryptHard(model.tarjeta)),
                R.color.colorAccent,
                R.color.colorFontLight,
                " "
            )
            holder.text.text = TextUtils.concat(name, "\n", fecha, "\n", tarjeta)
            holder.logo.setImageResource(R.drawable.icon_cruz_naranja)


        } else if (holder is HeaderViewHolder) {
            holder.header.text = "Altas"
        }
    }

    override fun getItemCount(): Int {
        return items.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (isPositionHeader(position)) TYPE_HEADER else TYPE_ITEM
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    fun getItem(pos: Int): HistorialAltaEntity {
        return items[pos - 1]
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val logo: ImageView = view.findViewById(R.id.historial_logo)
        val text: TextView = view.findViewById(R.id.historial_text)
    }

    class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val header: TextView = view.findViewById(R.id.text_header)
    }
}
