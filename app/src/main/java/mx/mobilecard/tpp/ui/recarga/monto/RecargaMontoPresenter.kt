package mx.mobilecard.tpp.ui.recarga.monto

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import mx.mobilecard.tpp.BuildConfig
import mx.mobilecard.tpp.data.local.user.UserRepository
import mx.mobilecard.tpp.data.network.tpp.RecargaRequest
import mx.mobilecard.tpp.data.network.tpp.TppAPI
import mx.mobilecard.tpp.ui.BasePresenter
import mx.mobilecard.tpp.utils.ErrorUtils
import mx.mobilecard.tpp.utils.StringUtil

/**
 * ADDCEL on 2019-11-26.
 */
interface RecargaMontoPresenter : BasePresenter {
    fun recarga(monto: Double, idTarjeta: Int)
}

class RecargaMontoPresenterImpl(
    val api: TppAPI,
    val repository: UserRepository,
    val compositeDisposable: CompositeDisposable = CompositeDisposable(),
    val view: RecargaMontoView
) : RecargaMontoPresenter {

    override fun recarga(monto: Double, idTarjeta: Int) {

        val request = repository.read()?.id?.let { RecargaRequest(it, idTarjeta, monto) }

        if (request != null) {

            view.showProgress()

            val rDisp = api.recarga(
                BuildConfig.ADDCEL_APP_ID,
                4,
                StringUtil.getCurrentLanguage(),
                TppAPI.TIPO_OPERADOR,
                request
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        view.hideProgress()
                        view.onRecarga(it)
                    }
                    , {
                        view.hideProgress()
                        view.onError(ErrorUtils.getFormattedHttpErrorMsg(it))
                    })

            addToDisposables(rDisp)
        } else {
            view.onError("El operador no ha iniciado sesión")
        }
    }

    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

}


