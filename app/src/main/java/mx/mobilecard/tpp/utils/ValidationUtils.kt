package mx.mobilecard.tpp.utils

import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.google.android.material.textfield.TextInputLayout


/**
 * ADDCEL on 21/11/19.
 */
object ValidationUtils {

    fun clearViewErrors(vararg views: View) {
        for (v in views) {
            if (v is TextInputLayout) v.error = null
            if (v is EditText) {
                v.error = null
                v.clearFocus()
            }
            if (v is TextView) {
                v.error = null
                v.clearFocus()
            }
        }
    }


}