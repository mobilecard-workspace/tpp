package mx.mobilecard.tpp.utils

import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.lang.reflect.Type


/**
 * ADDCEL on 2019-11-26.
 */
object JsonUtil {

    private val gson = GsonBuilder().serializeSpecialFloatingPointValues().create()

    @Synchronized
    fun toJson(o: Any): String? {
        return gson.toJson(o)
    }

    @Synchronized
    fun <T> fromJson(json: String, t: Class<T>): T {
        return gson.fromJson(json, t)
    }

    @Synchronized
    fun <T> fromJson(json: String, type: Type): T {
        return gson.fromJson(json, type)
    }

    @Synchronized
    fun <T> fromJson(reader: JsonReader, t: Class<T>): T {
        return gson.fromJson(reader, t)
    }

    @Synchronized
    fun isJson(s: String): Boolean {
        try {
            JSONObject(s)
        } catch (e: JSONException) {
            try {
                JSONArray(s)
            } catch (e1: JSONException) {
                return false
            }
        }
        return true
    }

}