package mx.mobilecard.tpp.data.network.tpp

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


/**
 * ADDCEL on 21/11/19.
 */

data class LoginRequest(
    @SerializedName("password")
    val password: String,
    @SerializedName("usuario")
    val usuario: String
)

data class LoginEntity(
    @SerializedName("card")
    val card: CardEntity,
    @SerializedName("id")
    val id: Int,
    @SerializedName("idError")
    val idError: Int,
    @SerializedName("mensajeError")
    val mensajeError: String,
    @SerializedName("nombre")
    val nombre: String,
    @SerializedName("status")
    val status: Int
)

data class CardEntity(
    @SerializedName("codigoCvv")
    val codigoCvv: String,
    @SerializedName("nombre")
    val nombre: String,
    @SerializedName("pan")
    val pan: String,
    @SerializedName("saldo")
    val saldo: Double,
    @SerializedName("vigencia")
    val vigencia: String
)

data class TebcaAltaRequest(
    @SerializedName("apellidoM")
    val apellidoM: String,
    @SerializedName("apellidoP")
    val apellidoP: String,
    @SerializedName("digitoVerificador")
    val digitoVerificador: String,
    @SerializedName("dni")
    val dni: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("fechaNac")
    val fechaNac: String,
    @SerializedName("genero")
    val genero: String,
    @SerializedName("nombres")
    val nombres: String,
    @SerializedName("pan")
    val pan: String,
    @SerializedName("vigencia")
    val vigencia: String,
    //NUEVOS CAMPOS
    @SerializedName("direccion")
    val direccion: String = "",
    @SerializedName("regionId")
    val regionId: String = "",
    @SerializedName("locationId")
    val locationId: String = "",
    @SerializedName("subLocationId")
    val subLocationId: String = "",
    @SerializedName("telefono")
    val telefono: String = "",
    @SerializedName("workCenter")
    val workCenter: String = "",
    @SerializedName("occupation")
    val occupation: String = "",
    @SerializedName("civilServant")
    val civilServant: String = "0",
    @SerializedName("publicOffice")
    val publicOffice: String = "",
    @SerializedName("publicInstitution")
    val publicInstitution: String = ""
)

@Parcelize
data class TebcaAltaResponse(
    @SerializedName("cards")
    val cards: List<TebcaCardEntity>,
    @SerializedName("idError")
    val idError: Int,
    @SerializedName("mensajeError")
    val mensajeError: String
) : Parcelable

@Parcelize
data class TebcaCardEntity(
    @SerializedName("fechaAlta")
    val fechaAlta: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("nombre")
    val nombre: String,
    @SerializedName("pan")
    val pan: String,
    @SerializedName("dni")
    val dni: String
) : Parcelable

data class RecargaRequest(
    @SerializedName("idOperador")
    val idOperador: Int,
    @SerializedName("idTarjeta")
    val idTarjeta: Int,
    @SerializedName("monto")
    val monto: Double
)

@Parcelize
data class RecargaEntity(
    @SerializedName("autorizacion")
    val autorizacion: String,
    @SerializedName("fecha")
    val fecha: String,
    @SerializedName("idError")
    val idError: Int,
    @SerializedName("mensajeError")
    val mensajeError: String,
    @SerializedName("monto")
    val monto: Double,
    @SerializedName("nombreDestino")
    val nombreDestino: String,
    @SerializedName("tarjejaDestino")
    val tarjejaDestino: String
) : Parcelable


data class HistorialAltaResponse(
    @SerializedName("data")
    val `data`: List<HistorialAltaEntity>,
    @SerializedName("idError")
    val idError: Int,
    @SerializedName("mensajeError")
    val mensajeError: String
)

data class HistorialAltaEntity(
    @SerializedName("fechaAlta")
    val fechaAlta: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("nombre")
    val nombre: String,
    @SerializedName("pan")
    val tarjeta: String
)

data class HistorialRecargaResponse(
    @SerializedName("data")
    val `data`: List<HistorialRecargaEntity>,
    @SerializedName("idError")
    val idError: Int,
    @SerializedName("mensajeError")
    val mensajeError: String
)

data class HistorialRecargaEntity(
    @SerializedName("fecha")
    val fecha: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("monto")
    val monto: Double,
    @SerializedName("nombre")
    val nombre: String,
    @SerializedName("tarjeta")
    val tarjeta: String
)